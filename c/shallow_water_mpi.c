/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include "mpi.h"
#include "shallow_water_mpi.h"

/* Use a leap-frog method to solve the linearized shallow-water
   equations - MPI version with a slab decomposition */  

int main(int argc, char* argv[]) {

  /* declarations */

  int m,jout,j,k,nsteps,it,exsol;
  int myid,numprocs,myup,mydown,mcol,mm; 
  double *h,*u,*v,*hold,*uold,*vold,*x,*y,*hus,*hur,*vus,*vur,*hds,*hdr,*vds,*vdr;
  double ttot,c,cfl,dx,dt,t,err,size,errloc,sizeloc,lam,lamcs,dtr,swap,kx,ky,uloc,hloc,vloc;
  MPI_Request request[8];
  MPI_Status statr[8]; 
  FILE* FID; 
  
  /* set parameters */

  m=2880;
  ttot=10.0;
  c=1.0;
  cfl=0.5;
  kx=1.0;
  ky=2.0; 
  jout=120;
  exsol=1; /* This is a logical variable =0 -> No exact solution available */ 

  /*  Set up the communicator */

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  /* For a slab decomposition we simply divide up the columns */

  mcol=m/numprocs;

  if (mcol < 3){printf("Too many processors for the problem size"); exit(1);}

  /* We must take account of the possibility that one slab
     is fatter than the rest - I will make it the last one */

  if (myid < numprocs-1) mm=mcol; else mm=m-mcol*(numprocs-1);

  /* Allocate arrays - one-dimensional but an index function is used to simplify 
     Notice that I use the fact that u and v are only differentied in one direction */
 

  h=malloc((m+2)*(mm+2)*sizeof(double));
  u=malloc((m+2)*(mm)*sizeof(double));
  v=malloc((m)*(mm+2)*sizeof(double));
  hold=malloc(m*mm*sizeof(double));
  uold=malloc(m*mm*sizeof(double));
  vold=malloc(m*mm*sizeof(double));
  x=malloc(m*sizeof(double));
  y=malloc(mm*sizeof(double));
  hus=malloc(m*sizeof(double));
  hur=malloc(m*sizeof(double));
  vus=malloc(m*sizeof(double));
  vur=malloc(m*sizeof(double));
  hds=malloc(m*sizeof(double));
  hdr=malloc(m*sizeof(double));
  vds=malloc(m*sizeof(double));
  vdr=malloc(m*sizeof(double));


  /* Find my neighbors - uses periodicity */

  if (myid == 0) mydown=numprocs-1; else mydown=myid-1;

  if (myid == numprocs-1) myup=0; else myup=myid+1;

  /* Set up the mesh and time step */ 

  dx=1.0/((double) m);
  dt=cfl*dx/c; 
  nsteps=(ttot/dt);
  dtr=ttot/nsteps;
  if (dtr > dt) {nsteps=nsteps+1; dt=ttot/nsteps;}
  else dt=dtr;

  lam=dt/dx; 
  lamcs=lam*c*c;

  for (j=0; j<m; j++) x[j]=(j+1)*dx;

  for (k=0; k<mm; k++) y[k]=(myid*mcol+k+1)*dx;  /* Use the fact that mm=mcol for myid < numprocs-1 */

  /* Save basic input information - we do all io on myid=0 */

  if (myid==0){
    FID=fopen("case1_mpi.txt","w");
    fprintf(FID, "m=%i\n",m);
    fprintf(FID, "nsteps=%i\n",nsteps);
    fprintf(FID, "ttot=%.16e\n",ttot);
    fprintf(FID, "c=%.16e\n",c);
    fprintf(FID, "kx=%.16e\n",kx);
    fprintf(FID, "ky=%.16e\n",ky);
    fclose(FID);
    FID=fopen("sw_mpi_error_data.case1","w");
  } 

  /* Initial data - note we need it at 2 time levels to get going
     but we assume that is taken care of in the ShallowWaterSol routine */

  for (k=0; k<mm; k++) {
    for (j=0; j<m; j++) {
      ShallowWaterSol(&h[id2p(j,k,m,mm)],&u[id2px(j,k,m,mm)],&v[id2py(j,k,m,mm)],x[j],y[k],0.0,kx,ky,c);
    }
  }


  for (k=0; k<mm; k++) {
    for (j=0; j<m; j++) {
      ShallowWaterSol(&hold[id2(j,k,m,mm)],&uold[id2(j,k,m,mm)],&vold[id2(j,k,m,mm)],x[j],y[k],-dt,kx,ky,c);
    }
  }

  /* Now march */

  for (it=0; it < nsteps; it++) {
    /* We begin by exchanging data - h and v with our up and down neighbors
       I am using nonblocking sends and receives */

    for (j=0; j<m; j++){
      hds[j]=h[id2p(j,0,m,mm)];
      hus[j]=h[id2p(j,mm-1,m,mm)];
      vds[j]=v[id2py(j,0,m,mm)];
      vus[j]=v[id2py(j,mm-1,m,mm)];
      hdr[j]=0.0;
      hur[j]=0.0;
      vdr[j]=0.0;
      vur[j]=0.0;
    }

    MPI_Irecv(hdr,m,MPI_DOUBLE,mydown,0,MPI_COMM_WORLD,&request[1]);
    MPI_Irecv(hur,m,MPI_DOUBLE,myup,1,MPI_COMM_WORLD,&request[2]);
    MPI_Irecv(vdr,m,MPI_DOUBLE,mydown,2,MPI_COMM_WORLD,&request[3]);
    MPI_Irecv(vur,m,MPI_DOUBLE,myup,3,MPI_COMM_WORLD,&request[4]);
    MPI_Isend(hds,m,MPI_DOUBLE,mydown,1,MPI_COMM_WORLD,&request[5]);      
    MPI_Isend(hus,m,MPI_DOUBLE,myup,0,MPI_COMM_WORLD,&request[6]);      
    MPI_Isend(vds,m,MPI_DOUBLE,mydown,3,MPI_COMM_WORLD,&request[7]);      
    MPI_Isend(vus,m,MPI_DOUBLE,myup,2,MPI_COMM_WORLD,&request[0]);      

    /* Compute what we can while the communication proceeds */

    for (k=1; k < mm-1; k++) {
      h[id2p(-1,k,m,mm)]=h[id2p(m-1,k,m,mm)];
      h[id2p(m,k,m,mm)]=h[id2p(0,k,m,mm)];
      u[id2px(-1,k,m,mm)]=u[id2px(m-1,k,m,mm)];
      u[id2px(m,k,m,mm)]=u[id2px(0,k,m,mm)];
      for (j=0; j < m; j++) {
	hold[id2(j,k,m,mm)] -= lam*(u[id2px(j+1,k,m,mm)]-u[id2px(j-1,k,m,mm)]+v[id2py(j,k+1,m,mm)]-v[id2py(j,k-1,m,mm)]);
	uold[id2(j,k,m,mm)] -= lamcs*(h[id2p(j+1,k,m,mm)]-h[id2p(j-1,k,m,mm)]);
	vold[id2(j,k,m,mm)] -= lamcs*(h[id2p(j,k+1,m,mm)]-h[id2p(j,k-1,m,mm)]); 
      } 
    }

    /* Now we must wait for the communications to complete */

    MPI_Waitall(8,request,statr);

    for (j=0; j<m; j++){
      h[id2p(j,-1,m,mm)]=hdr[j];
      h[id2p(j,mm,m,mm)]=hur[j];
      v[id2py(j,-1,m,mm)]=vdr[j];
      v[id2py(j,mm,m,mm)]=vur[j];
    }

    h[id2p(-1,0,m,mm)]=h[id2p(m-1,0,m,mm)];
    h[id2p(m,0,m,mm)]=h[id2p(0,0,m,mm)];
    u[id2px(-1,0,m,mm)]=u[id2px(m-1,0,m,mm)];
    u[id2px(m,0,m,mm)]=u[id2px(0,0,m,mm)];
    h[id2p(-1,mm-1,m,mm)]=h[id2p(m-1,mm-1,m,mm)];
    h[id2p(m,mm-1,m,mm)]=h[id2p(0,mm-1,m,mm)];
    u[id2px(-1,mm-1,m,mm)]=u[id2px(m-1,mm-1,m,mm)];
    u[id2px(m,mm-1,m,mm)]=u[id2px(0,mm-1,m,mm)];

    for (j=0; j < m; j++) {
      hold[id2(j,0,m,mm)] -= lam*(u[id2px(j+1,0,m,mm)]-u[id2px(j-1,0,m,mm)]+v[id2py(j,1,m,mm)]-v[id2py(j,-1,m,mm)]);
      uold[id2(j,0,m,mm)] -= lamcs*(h[id2p(j+1,0,m,mm)]-h[id2p(j-1,0,m,mm)]);
      vold[id2(j,0,m,mm)] -= lamcs*(h[id2p(j,1,m,mm)]-h[id2p(j,-1,m,mm)]); 
      hold[id2(j,mm-1,m,mm)] -= lam*(u[id2px(j+1,mm-1,m,mm)]-u[id2px(j-1,mm-1,m,mm)]+v[id2py(j,mm,m,mm)]-v[id2py(j,mm-2,m,mm)]);
      uold[id2(j,mm-1,m,mm)] -= lamcs*(h[id2p(j+1,mm-1,m,mm)]-h[id2p(j-1,mm-1,m,mm)]);
      vold[id2(j,mm-1,m,mm)] -= lamcs*(h[id2p(j,mm,m,mm)]-h[id2p(j,mm-2,m,mm)]); 
    }
    

  /* Swap */

    for (k=0; k < mm; k++) {
      for (j=0; j < m; j++) {
	swap=hold[id2(j,k,m,mm)];
	hold[id2(j,k,m,mm)]=h[id2p(j,k,m,mm)];
	h[id2p(j,k,m,mm)]=swap;
	swap=uold[id2(j,k,m,mm)];
	uold[id2(j,k,m,mm)]=u[id2px(j,k,m,mm)];
	u[id2px(j,k,m,mm)]=swap;
	swap=vold[id2(j,k,m,mm)];
	vold[id2(j,k,m,mm)]=v[id2py(j,k,m,mm)];
	v[id2py(j,k,m,mm)]=swap;
      }
    }

    /* Check for output */ 

    if (((it+1)%jout)==0) {
      t=(it+1)*dt;
      if (exsol) {
	errloc=0.0;
	sizeloc=0.0;
	for (k=0; k<mm; k++) {
	  for (j=0; j<m; j++) {
	    ShallowWaterSol(&hloc,&uloc,&vloc,x[j],y[k],t,kx,ky,c);
	    errloc += dx*dx*(c*c*(h[id2p(j,k,m,mm)]-hloc)*(h[id2p(j,k,m,mm)]-hloc)
			  +(u[id2px(j,k,m,mm)]-uloc)*(u[id2px(j,k,m,mm)]-uloc)+(v[id2py(j,k,m,mm)]-vloc)*(v[id2py(j,k,m,mm)]-vloc));
	    sizeloc += dx*dx*(c*c*hloc*hloc+uloc*uloc+vloc*vloc);
	  }
	}
	MPI_Reduce(&errloc,&err,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if (myid == 0) 	fprintf(FID,"%.16e %.16e\n",t,sqrt(err/size)); 
      }
      else {
	sizeloc=0.0;
	for (k=0; k<mm; k++) {
	  for (j=0; j<m; j++) {
	    sizeloc += dx*dx*(c*c*h[id2p(j,k,m,mm)]*h[id2p(j,k,m,mm)]+u[id2px(j,k,m,mm)]*u[id2px(j,k,m,mm)]+v[id2py(j,k,m,mm)]*v[id2py(j,k,m,mm)]);
	  }
	}
	MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if (myid == 0) fprintf(FID,"%.16e %.16e\n",t,sqrt(size)); 
      }
    }
  }
  if (myid == 0) fclose(FID);
  MPI_Finalize();
}


