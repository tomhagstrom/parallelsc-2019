/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h> 
#include <cufft.h>
#include <cuComplex.h> 

#define NXev 2048 
#define BATCH 1

void f(double *y, double x){
  double pi=3.1415926535897932385;
  *y=exp(-20.0*cos(2.0*pi*x)*cos(2.0*pi*x));
}

__global__ void Extend(cufftDoubleComplex *fhat, cufftDoubleComplex *fhatx, int N, int Nx) {

  int j = blockIdx.x * blockDim.x + threadIdx.x;
  if (j < (Nx/2+1)) {
    if (j < (N/2+1)){
      fhatx[j]=cuCdiv(fhat[j],make_cuDoubleComplex((double) N,0.0));
      }
    else {
      fhatx[j]=make_cuDoubleComplex(0.0,0.0);
      }
   }
}


int main(int argc, char **argv) {
cufftHandle planf,planix;
cufftDoubleComplex *fhat,*fhatx;
cufftDoubleReal *fdat,*fint;
double *fint_host,*fdat_host;  
double x,err,errloc,y;
int N,q,j; 

cudaMalloc((void**)&fhatx, sizeof(cufftDoubleComplex)*(NXev/2+1)*BATCH);
if (cudaGetLastError() != cudaSuccess){
	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
}

cudaMalloc((void**)&fint, sizeof(cufftDoubleReal)*NXev*BATCH);
if (cudaGetLastError() != cudaSuccess){
	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
}

fint_host=(double *) malloc(NXev*sizeof(double));

if (cufftPlan1d(&planix, NXev, CUFFT_Z2D, BATCH) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: Plan creation failed");
	return (-1);	
}	

for (q=3; q<8; q++){
  N=pow(2,q);
  cudaMalloc((void**)&fhat, sizeof(cufftDoubleComplex)*(N/2+1)*BATCH);
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  cudaMalloc((void**)&fdat, sizeof(cufftDoubleReal)*N*BATCH);
  if (cudaGetLastError() != cudaSuccess){
	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  if (cufftPlan1d(&planf, N, CUFFT_D2Z, BATCH) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: Plan creation failed");
	return (-1);	
  }
  fdat_host=(double *) malloc(N*sizeof(double));
  for (j=0; j<N; j++){
    x=((double) j)/((double) N);
    f(&y,x);
    fdat_host[j]=y;
    }
  cudaMemcpy(fdat,fdat_host,N*sizeof(double),cudaMemcpyHostToDevice); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to copy\n");
	return (-1);	
  }

/* Forward transform */

  if (cufftExecD2Z(planf, fdat, fhat) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: ExecD2Z Forward failed");
	return (-1);	
  }
  
  if (cudaDeviceSynchronize() != cudaSuccess){
    	fprintf(stderr, "Cuda error: Failed to synchronize\n");
	return (-1);	
  }

/* Extend the data and normalize */

  Extend<<<6,200>>>(fhat,fhatx,N,NXev); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to extend\n");
	return (-1);	
  }

  if (cudaDeviceSynchronize() != cudaSuccess){
    	fprintf(stderr, "Cuda error: Failed to synchronize\n");
	return (-1);	
  }

/* Inverse transform */

  if (cufftExecZ2D(planix, fhatx, fint) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: ExecZ2D Inverse failed");
	return (-1);	
  }

  cudaMemcpy(fint_host,fint,NXev*sizeof(double),cudaMemcpyDeviceToHost); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to copy\n");
	return (-1);	
  }

/* Check error */

  err=0.0;
  for (j=0; j<NXev; j++){
    x=((double) j)/((double) NXev);
    f(&y,x);
    errloc=fabs(y-fint_host[j]);
    if (errloc > err) err=errloc;
    }
  printf("N=%i err=%.4e\n",N,err); 

  cufftDestroy(planf);
  cudaFree(fhat);
  cudaFree(fdat);
  free(fdat_host); 
}

cufftDestroy(planix);
cudaFree(fint);
cudaFree(fhatx); 
free(fint_host);

return 0;

}


