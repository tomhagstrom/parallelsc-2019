/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include "mpi.h"
#include "shallow_water_mpi.h"

/* Use a leap-frog method to solve the linearized shallow-water
   equations - MPI version with a slab decomposition */  

int main(int argc, char* argv[]) {

  /* declarations */

  int m,jout,j,k,nsteps,it,exsol;
  int myid,numprocs,myup,mydown,myleft,myright,mcol,myxl,myyd,mmx,mmy,leftover;  
  double *h,*u,*v,*hold,*uold,*vold,*x,*y,*hus,*hur,*vus,*vur,*hds,*hdr,*vds,*vdr;
  double *hls,*hlr,*hrs,*hrr,*uls,*ulr,*urs,*urr; 
  double ttot,c,cfl,dx,dy,dt,t,err,size,errloc,sizeloc,lam,lamcs,dtr,swap,kx,ky,uloc,hloc,vloc;
  double comptime,communtime,iotime,tc1,tc2,ti1,ti2,tt1,tt2; 
  MPI_Request request[16];
  MPI_Status statr[16];
  MPI_Comm newcomm;
  int dims[2],mycoords[2],isperiodic[2],reorder,ierr;
  FILE* FID; 
  
  /* set parameters */

  m=800;
  ttot=10.0;
  c=1.0;
  cfl=0.5;
  kx=1.0;
  ky=2.0; 
  jout=40;
  exsol=1; /* This is a logical variable =0 -> No exact solution available */ 

  /*  Set up the communicator */

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  /* Now use the MPI Cartesian functions to decompose the domain */

  isperiodic[0]=1;
  isperiodic[1]=1;
  reorder=1;
  dims[0]=0;
  dims[1]=0;
  ierr=MPI_Dims_create(numprocs,2,dims);
  ierr=MPI_Cart_create(MPI_COMM_WORLD,2,dims,isperiodic,reorder,&newcomm);

  /* Neighbors and coordinates */

  ierr=MPI_Comm_rank(newcomm,&myid);
  ierr=MPI_Cart_shift(newcomm,0,1,&myleft,&myright);
  ierr=MPI_Cart_shift(newcomm,1,1,&mydown,&myup);
  ierr=MPI_Cart_coords(newcomm,myid,2,mycoords); 
  
  /* Now we divvy out the data - I will improve on what I did in the first code
     which could cause the last processor to be overloaded - here I will determine
     left and right indices and up and down indices for each process */

  leftover=m%dims[0]; 
  mmx=m/dims[0];

  if (mmx < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[0] < leftover+1){myxl=(mmx+1)*mycoords[0]+1;}
  else {myxl=(mmx+1)*leftover+mmx*(mycoords[0]-leftover)+1;}

  if (mycoords[0] < leftover){mmx=mmx+1;}

  leftover=m%dims[1]; 
  mmy=m/dims[1];

  if (mmy < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[1] < leftover+1){myyd=(mmy+1)*mycoords[1]+1;}
  else {myyd=(mmy+1)*leftover+mmy*(mycoords[1]-leftover)+1;}

  if (mycoords[1] < leftover){mmy=mmy+1;}

  h=malloc((mmx+2)*(mmy+2)*sizeof(double));
  u=malloc((mmx+2)*(mmy)*sizeof(double));
  v=malloc((mmx)*(mmy+2)*sizeof(double));
  hold=malloc(mmx*mmy*sizeof(double));
  uold=malloc(mmx*mmy*sizeof(double));
  vold=malloc(mmx*mmy*sizeof(double));
  x=malloc(mmx*sizeof(double));
  y=malloc(mmy*sizeof(double));
  hus=malloc(mmx*sizeof(double));
  hur=malloc(mmx*sizeof(double));
  vus=malloc(mmx*sizeof(double));
  vur=malloc(mmx*sizeof(double));
  hds=malloc(mmx*sizeof(double));
  hdr=malloc(mmx*sizeof(double));
  vds=malloc(mmx*sizeof(double));
  vdr=malloc(mmx*sizeof(double));
  hrs=malloc(mmy*sizeof(double));
  hrr=malloc(mmy*sizeof(double));
  urs=malloc(mmy*sizeof(double));
  urr=malloc(mmy*sizeof(double));
  hls=malloc(mmy*sizeof(double));
  hlr=malloc(mmy*sizeof(double));
  uls=malloc(mmy*sizeof(double));
  ulr=malloc(mmy*sizeof(double));

  /* Set up the mesh and time step */ 

  dx=1.0/((double) m);
  dy=dx;
  dt=cfl*dx/c; 
  nsteps=(ttot/dt);
  dtr=ttot/nsteps;
  if (dtr > dt) {nsteps=nsteps+1; dt=ttot/nsteps;}
  else dt=dtr;

  lam=dt/dx; 
  lamcs=lam*c*c;

  for (j=0; j<mmx; j++) x[j]=(j+myxl)*dx;

  for (k=0; k<mmy; k++) y[k]=(k+myyd)*dy;

  /* Save basic input information - we do all io on myid=0 */

  if (myid==0){
    FID=fopen("case1_mpi_Cart.txt","w");
    fprintf(FID, "m=%i\n",m);
    fprintf(FID, "nsteps=%i\n",nsteps);
    fprintf(FID, "ttot=%.16e\n",ttot);
    fprintf(FID, "c=%.16e\n",c);
    fprintf(FID, "kx=%.16e\n",kx);
    fprintf(FID, "ky=%.16e\n",ky);
    fclose(FID);
    FID=fopen("sw_mpi_Cart_error_data.case1","w");
  } 

  /* Initial data - note we need it at 2 time levels to get going
     but we assume that is taken care of in the ShallowWaterSol routine */

  for (k=0; k<mmy; k++) {
    for (j=0; j<mmx; j++) {
      ShallowWaterSol(&h[id2p(j,k,mmx,mmy)],&u[id2px(j,k,mmx,mmy)],&v[id2py(j,k,mmx,mmy)],
		      x[j],y[k],0.0,kx,ky,c);
    }
  }


  for (k=0; k<mmy; k++) {
    for (j=0; j<mmx; j++) {
      ShallowWaterSol(&hold[id2(j,k,mmx,mmy)],&uold[id2(j,k,mmx,mmy)],&vold[id2(j,k,mmx,mmy)],
		      x[j],y[k],-dt,kx,ky,c);
    }
  }

  /* Now march - start timers */

  comptime=0.0;
  iotime=0.0;
  communtime=0.0; 

  for (it=0; it < nsteps; it++) {
    /* We begin by exchanging data - h and v with our up and down neighbors
       h and u with our left and right neighbors
       I am using nonblocking sends and receives */

    tt1=MPI_Wtime();
    
    for (j=0; j<mmx; j++){
      hds[j]=h[id2p(j,0,mmx,mmy)];
      hus[j]=h[id2p(j,mmy-1,mmx,mmy)];
      vds[j]=v[id2py(j,0,mmx,mmy)];
      vus[j]=v[id2py(j,mmy-1,mmx,mmy)];
      hdr[j]=0.0;
      hur[j]=0.0;
      vdr[j]=0.0;
      vur[j]=0.0;
    }

    for (k=0; k<mmy; k++){
      hls[k]=h[id2p(0,k,mmx,mmy)];
      hrs[k]=h[id2p(mmx-1,k,mmx,mmy)];
      uls[k]=u[id2px(0,k,mmx,mmy)];
      urs[k]=u[id2px(mmx-1,k,mmx,mmy)];
      hlr[k]=0.0;
      hrr[k]=0.0;
      ulr[k]=0.0;
      urr[k]=0.0;
    }

    MPI_Irecv(hdr,mmx,MPI_DOUBLE,mydown,0,newcomm,&request[0]);
    MPI_Irecv(hur,mmx,MPI_DOUBLE,myup,1,newcomm,&request[1]);
    MPI_Irecv(vdr,mmx,MPI_DOUBLE,mydown,2,newcomm,&request[2]);
    MPI_Irecv(vur,mmx,MPI_DOUBLE,myup,3,newcomm,&request[3]);
    MPI_Irecv(hlr,mmy,MPI_DOUBLE,myleft,4,newcomm,&request[4]);
    MPI_Irecv(hrr,mmy,MPI_DOUBLE,myright,5,newcomm,&request[5]);
    MPI_Irecv(ulr,mmy,MPI_DOUBLE,myleft,6,newcomm,&request[6]);
    MPI_Irecv(urr,mmy,MPI_DOUBLE,myright,7,newcomm,&request[7]);
    MPI_Isend(hds,mmx,MPI_DOUBLE,mydown,1,newcomm,&request[8]);      
    MPI_Isend(hus,mmx,MPI_DOUBLE,myup,0,newcomm,&request[9]);      
    MPI_Isend(vds,mmx,MPI_DOUBLE,mydown,3,newcomm,&request[10]);      
    MPI_Isend(vus,mmx,MPI_DOUBLE,myup,2,newcomm,&request[11]);      
    MPI_Isend(hls,mmy,MPI_DOUBLE,myleft,5,newcomm,&request[12]);      
    MPI_Isend(hrs,mmy,MPI_DOUBLE,myright,4,newcomm,&request[13]);      
    MPI_Isend(uls,mmy,MPI_DOUBLE,myleft,7,newcomm,&request[14]);      
    MPI_Isend(urs,mmy,MPI_DOUBLE,myright,6,newcomm,&request[15]);      

    /* Now we must wait for the communications to complete */

    MPI_Waitall(16,request,statr);

    for (j=0; j<mmx; j++){
      h[id2p(j,-1,mmx,mmy)]=hdr[j];
      h[id2p(j,mmy,mmx,mmy)]=hur[j];
      v[id2py(j,-1,mmx,mmy)]=vdr[j];
      v[id2py(j,mmy,mmx,mmy)]=vur[j];
    }

    for (k=0; k<mmy; k++){
      h[id2p(-1,k,mmx,mmy)]=hlr[k];
      h[id2p(mmx,k,mmx,mmy)]=hrr[k];
      u[id2px(-1,k,mmx,mmy)]=ulr[k];
      u[id2px(mmx,k,mmx,mmy)]=urr[k];
    }

    tt2=MPI_Wtime();
    communtime=communtime+tt2-tt1;

    tc1=MPI_Wtime();

    for (k=0; k<mmy; k++){
      for (j=0; j<mmx; j++){
	hold[id2(j,k,mmx,mmy)] -= lam*(u[id2px(j+1,k,mmx,mmy)]-u[id2px(j-1,k,mmx,mmy)]
				       +v[id2py(j,k+1,mmx,mmy)]-v[id2py(j,k-1,mmx,mmy)]);
	uold[id2(j,k,mmx,mmy)] -= lamcs*(h[id2p(j+1,k,mmx,mmy)]-h[id2p(j-1,k,mmx,mmy)]);
	vold[id2(j,k,mmx,mmy)] -= lamcs*(h[id2p(j,k+1,mmx,mmy)]-h[id2p(j,k-1,mmx,mmy)]); 
      } 
    }

  /* Swap */

    for (k=0; k < mmy; k++) {
      for (j=0; j < mmx; j++) {
	swap=hold[id2(j,k,mmx,mmy)];
	hold[id2(j,k,mmx,mmy)]=h[id2p(j,k,mmx,mmy)];
	h[id2p(j,k,mmx,mmy)]=swap;
	swap=uold[id2(j,k,mmx,mmy)];
	uold[id2(j,k,mmx,mmy)]=u[id2px(j,k,mmx,mmy)];
	u[id2px(j,k,mmx,mmy)]=swap;
	swap=vold[id2(j,k,mmx,mmy)];
	vold[id2(j,k,mmx,mmy)]=v[id2py(j,k,mmx,mmy)];
	v[id2py(j,k,mmx,mmy)]=swap;
      }
    }

    tc2=MPI_Wtime();
    comptime=comptime+tc2-tc1; 

    /* Check for output */ 

    if (((it+1)%jout)==0) {

      ti1=MPI_Wtime();
      
      t=(it+1)*dt;
      if (exsol) {
	errloc=0.0;
	sizeloc=0.0;
	for (k=0; k<mmy; k++) {
	  for (j=0; j<mmx; j++) {
	    ShallowWaterSol(&hloc,&uloc,&vloc,x[j],y[k],t,kx,ky,c);
	    errloc += dx*dy*(c*c*(h[id2p(j,k,mmx,mmy)]-hloc)*(h[id2p(j,k,mmx,mmy)]-hloc)
			  +(u[id2px(j,k,mmx,mmy)]-uloc)*(u[id2px(j,k,mmx,mmy)]-uloc)
			  +(v[id2py(j,k,mmx,mmy)]-vloc)*(v[id2py(j,k,mmx,mmy)]-vloc));
	    sizeloc += dx*dy*(c*c*hloc*hloc+uloc*uloc+vloc*vloc);
	  }
	}
	MPI_Reduce(&errloc,&err,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
	MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
	if (myid == 0) 	fprintf(FID,"%.16e %.16e\n",t,sqrt(err/size)); 
      }
      else {
	sizeloc=0.0;
	for (k=0; k<mmy; k++) {
	  for (j=0; j<mmx; j++) {
	    sizeloc += dx*dy*(c*c*h[id2p(j,k,mmx,mmy)]*h[id2p(j,k,mmx,mmy)]
			      +u[id2px(j,k,mmx,mmy)]*u[id2px(j,k,mmx,mmy)]
			      +v[id2py(j,k,mmx,mmy)]*v[id2py(j,k,mmx,mmy)]);
	  }
	}
	MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
	if (myid == 0) fprintf(FID,"%.16e %.16e\n",t,sqrt(size)); 
      }

      ti2=MPI_Wtime();
      iotime=iotime+ti2-ti1;
      
    }
  }
  if (myid == 0){
    fclose(FID);
    printf("Computation time=%.16e\n",comptime);
    printf("IO time=%.16e\n",iotime);
    printf("Communication time=%.16e\n",communtime);
  }
  MPI_Finalize();
}


