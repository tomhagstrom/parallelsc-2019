// macros to map a 2D index to 1d address space with Fortran ordering 
#define id2(i,j,m,mm)   ((j)*(m)+(i))
#define id2p(i,j,m,mm)  ((j+1)*(m+2)+(i+1))
#define id2px(i,j,m,mm)  ((j)*(m+2)+(i+1))
#define id2py(i,j,m,mm)  ((j+1)*(m)+(i))

// Prototypes
void ShallowWaterSol(double* hloc, double* uloc, double* vloc, double x, 
		double y, double t, double kx, double ky, double c);



