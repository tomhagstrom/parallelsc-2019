#include <stdlib.h>
#include <math.h>

void Source(double* f, double x, double y, double z){
  double bg,bx,by,bz,sx,cx,sy,cy,sz,cz,pi,kx,ky,kz;
  pi=3.1415926535897932385;
  kx=2.0;
  ky=4.0;
  kz=3.0;
  cx=cos(pi*kx*x);
  sx=sin(pi*kx*x);
  cy=cos(pi*ky*y);
  sy=sin(pi*ky*y);
  cz=cos(pi*kz*z);
  sz=sin(pi*kz*z);
  bg=10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.75)*(y-.75)-7.0*(z-.5)*(z-.5));
  bx=-10.0*(x-.75)*bg;
  by=-2.0*(y-.75)*bg;
  bz=-14.0*(z-.5)*bg;
  *f=bg*pi*pi*(kx*kx+ky*ky+kz*kz)*sx*sy*sz-bx*pi*kx*cx*sy*sz
    -by*pi*ky*sx*cy*sz-bz*pi*kz*sx*sy*cz;
}

void diffun(double sigx[], double sigy[], double sigz[],
	    double x, double y, double z, double dx, double dy, double dz){
  sigx[0]=10.0*exp(-5.0*(x-.5*dx-.75)*(x-.5*dx-.75)-(y-.75)*(y-.75)-7.0*(z-.5)*(z-.5));
  sigx[1]=10.0*exp(-5.0*(x+.5*dx-.75)*(x+.5*dx-.75)-(y-.75)*(y-.75)-7.0*(z-.5)*(z-.5));
  sigy[0]=10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.5*dy-.75)*(y-.5*dy-.75)-7.0*(z-.5)*(z-.5));
  sigy[1]=10.0*exp(-5.0*(x-.75)*(x-.75)-(y+.5*dy-.75)*(y+.5*dy-.75)-7.0*(z-.5)*(z-.5));
  sigz[0]=10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.75)*(y-.75)-7.0*(z-.5*dz-.5)*(z-.5*dz-.5));
  sigz[1]=10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.75)*(y-.75)-7.0*(z+.5*dz-.5)*(z+.5*dz-.5));
}

void LaplaceSol(double *u, double x, double y, double z){
  double pi,kx,ky,kz;
  pi=3.1415926535897932385;
  kx=2.0;
  ky=4.0;
  kz=3.0;
  *u=sin(pi*kx*x)*sin(pi*ky*y)*sin(pi*kz*z);
}


