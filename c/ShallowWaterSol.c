#include <stdlib.h>
#include <math.h>

void ShallowWaterSol(double* h, double* u, double* v, double x, double y,
		     double t, double kx, double ky, double c) {

  double pi,kt,cx,cy,sx,sy,st,ct;
  pi=3.1415926535897932385;
  kt=c*sqrt(kx*kx+ky*ky);
  cx=cos(2.0*pi*kx*x);
  sx=sin(2.0*pi*kx*x);
  cy=cos(2.0*pi*ky*y);
  sy=sin(2.0*pi*ky*y);
  ct=cos(2.0*pi*kt*t);
  st=sin(2.0*pi*kt*t);
  *h=cx*cy*ct;
  *u=(c*c*kx/kt)*sx*cy*st;
  *v=(c*c*ky/kt)*cx*sy*st;
}



