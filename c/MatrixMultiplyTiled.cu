/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define TILE_WIDTH 16 

// Compute C = A * B
// Sgemm stands for single precision general matrix-matrix multiply
__global__ void sgemm(float *A, float *B, float *C, int numARows,
                      int numAColumns, int numBRows, int numBColumns) {

  __shared__ float ds_M[TILE_WIDTH][TILE_WIDTH];
  __shared__ float ds_N[TILE_WIDTH][TILE_WIDTH];
  int bx = blockIdx.x, by = blockIdx.y, tx = threadIdx.x, ty = threadIdx.y,
      Row = by * TILE_WIDTH + ty, Col = bx * TILE_WIDTH + tx;
  float Pvalue = 0;

  for (int m = 0; m < (numAColumns - 1) / TILE_WIDTH + 1; ++m) {
    if (Row < numARows && m * TILE_WIDTH + tx < numAColumns)
      ds_M[ty][tx] = A[Row * numAColumns + m * TILE_WIDTH + tx];
    else
      ds_M[ty][tx] = 0;
    if (Col < numBColumns && m * TILE_WIDTH + ty < numBRows)
      ds_N[ty][tx] = B[(m * TILE_WIDTH + ty) * numBColumns + Col];
    else
      ds_N[ty][tx] = 0;

    __syncthreads();
    for (int k = 0; k < TILE_WIDTH; ++k)
      Pvalue += ds_M[ty][k] * ds_N[k][tx];
    __syncthreads();
  }
  if (Row < numARows && Col < numBColumns)
    C[Row * numBColumns + Col] = Pvalue;
}

int main(int argc, char **argv) {
  float *hostA; // The A matrix
  float *hostB; // The B matrix
  float *hostC; // The output C matrix
  float *hostCD; // The output C matrix as computed on the device 
  float *deviceA;
  float *deviceB;
  float *deviceC;
  int numARows=33;    // number of Rows in the matrix A
  int numAColumns=65; // number of columns in the matrix A
  int numBRows=numAColumns;    // number of Rows in the matrix B
  int numBColumns=81; // number of columns in the matrix B
  int numCRows=numARows;
  int numCColumns=numBColumns;
  float pi=3.14159265;
  float x,t,diff,ldiff,sum;
  int i,j,k;

// Allocate memory on host

  hostA = (float *)malloc(numARows*numAColumns*sizeof(float));
  hostB = (float *)malloc(numBRows*numBColumns*sizeof(float));
  hostC = (float *)malloc(numCRows*numCColumns*sizeof(float));
  hostCD = (float *)malloc(numCRows*numCColumns*sizeof(float));

// Set matrix entries

  for (i=0; i<numARows; i++){
    t=pi*((float) i)/((float) numARows-1);
    for (j=0; j<numAColumns; j++){ 
      x=((float) j)/((float) numAColumns-1);
      hostA[i*numAColumns+j]=cos(x*t);
    }
  }
  for (j=0; j<numBRows; j++){
    x=((float) j)/((float) numBRows-1);
    for (k=0; k<numBColumns; k++){ 
      hostB[j*numBColumns+k]=pow(x,k);
    }
  }
  
// Compute the product for checking

  for (i=0; i<numARows; i++){
    for (k=0; k<numBColumns; k++){ 
      sum=0.0;
      for (j=0; j<numAColumns; j++){
        sum += hostA[i*numAColumns+j]*hostB[j*numBColumns+k];
      }
      hostC[i*numCColumns+k]=sum;
    }
  }
  
  // Allocate GPU memory here
  cudaMalloc((void **)&deviceA,numARows*numAColumns*sizeof(float));
  cudaMalloc((void **)&deviceB,numBRows*numBColumns*sizeof(float));
  cudaMalloc((void **)&deviceC,numCRows*numCColumns*sizeof(float));

  // Copy memory to the GPU here
  cudaMemcpy(deviceA,hostA,numARows*numAColumns*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(deviceB,hostB,numBRows*numBColumns*sizeof(float),cudaMemcpyHostToDevice);

  // Initialize the grid and block dimensions here
  dim3 blockDim(16, 16);
  dim3 gridDim(ceil(((float)numCColumns) / blockDim.x),
               ceil(((float)numCRows) / blockDim.y));

  // Launch the GPU Kernel here
  sgemm<<<gridDim, blockDim>>>(deviceA, deviceB, deviceC, numARows,
                               numAColumns, numBRows, numBColumns);
  cudaDeviceSynchronize();

  // Copy the GPU memory back to the CPU here

  cudaMemcpy(hostCD,deviceC,numCRows*numCColumns*sizeof(float),cudaMemcpyDeviceToHost);

  // Check to see that the answers are the same

  diff=0.0;
  for (i=0; i<numCRows; i++){
    for (j=0; j<numCColumns; j++){ 
      ldiff=fabs(hostC[i*numCColumns+j]-hostCD[i*numCColumns+j]);
      if (ldiff > diff) diff=ldiff;
    }
  }

  printf("The maximum difference is %.8e\n",diff);

  // Free the GPU memory here
  cudaFree(deviceA);
  cudaFree(deviceB);
  cudaFree(deviceC);

  free(hostA);
  free(hostB);
  free(hostC);
  free(hostCD); 

  return 0;
}
