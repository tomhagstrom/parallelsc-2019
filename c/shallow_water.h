// macros to map a 2D index to 1d address space with Fortran ordering 
#define id2(i,j,m)   ((j)*(m)+(i))
#define id2p(i,j,m)  ((j+1)*(m+2)+(i+1))

// Prototypes
void ShallowWaterSol(double* hloc, double* uloc, double* vloc, double x, 
		double y, double t, double kx, double ky, double c);



