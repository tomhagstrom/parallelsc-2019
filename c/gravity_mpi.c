/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include <omp.h>
#include "mpi.h"


/* Solve for the motion of N bodies using Newton's laws */

void Force(int myN, int N, double myx[], double myy[], double myz[], double x[], double y[], double z[], double m[],
	   double Fx[], double Fy[], double Fz[]){
  /* Calculate forces by Newton's Laws */
  double dx,dy,dz,rad,ris,small;
  int j,k;
  small=1e-20;  /* to avoid ifs in loops */ 
#pragma omp parallel for private(dx,dy,dz,rad,k,ris) shared(myx,myy,myz,x,y,z,m,Fx,Fy,Fz)
  for (j=0; j<myN; j++){
    Fx[j]=0.0;
    Fy[j]=0.0;
    Fz[j]=0.0;
    for (k=0; k<N; k++){
      dx=x[k]-myx[j];
      dy=y[k]-myy[j];
      dz=z[k]-myz[j];
      rad=sqrt(dx*dx+dy*dy+dz*dz+small);
      ris=m[k]/(rad*rad*rad);
      Fx[j] += dx*ris;
      Fy[j] += dy*ris;
      Fz[j] += dz*ris;
    }
  }
}

void Initialize(int myN, int myid, double myx[], double myy[], double myz[], double Vx[], double Vy[], double Vz[], double mym[]){
  /* Initial positions, velocities, and masses - to parallelize I need to set a different seed for rand */
    int j,rnum; /* Note - this is not a good random number generator but I don't care if these values are truly random */
    double rad,theta,phi;
    double pi=3.14159265358979;
    double mbar=5.0;  /* average mass */
    double Vbar=3.0;  /* mean initial outward velocity */
    double R=5.0; /* masses initially in a sphere of radius R */
    srand(myid*223+1); 
#pragma omp parallel for private(rnum,rad,theta,phi) shared(myx,myy,myz,Vx,Vy,Vz,mym)
  for (j=0; j<myN; j++){
    rnum=rand();
    rad=5.0*((double) (rnum))/((double) (RAND_MAX));
    rnum=rand();
    theta=pi*((double) (rnum))/((double) (RAND_MAX));
    rnum=rand();
    phi=2.0*pi*((double) (rnum))/((double) (RAND_MAX));
    myx[j]=rad*sin(theta)*cos(phi);
    myy[j]=rad*sin(theta)*sin(phi);
    myz[j]=rad*cos(theta); 
    rnum=rand();
    Vx[j]=Vbar*myx[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    Vy[j]=Vbar*myy[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    Vz[j]=Vbar*myz[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    mym[j]=mbar+(8.0*((double) (rnum))/((double) (RAND_MAX))-4.0);
  }
}

int main(int argc, char* argv[]) {

  /* declarations */

  int j,N,it,nsteps,myN,leftover;
  double *x, *y, *z, *Vx, *Vy, *Vz, *m, *Fx, *Fy, *Fz, *myx, *myy, *myz, *mym;
  int *counts;
  int *disps; 
  double ttot,dt,comp_time;
  FILE* FID;
  clock_t start_time,finish_time;
  int myid,numprocs;
  
  
  /* set parameters */

  N=10000;
  ttot=10.0;
  nsteps=10000;
  dt=ttot/nsteps;

    /*  Set up the communicator */

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  /* Break up the problem */

  leftover=N%numprocs; 
  myN=N/numprocs;

  if (myid < leftover){myN=myN+1;}


  /* Allocate arrays */

  x=malloc(N*sizeof(double));
  y=malloc(N*sizeof(double));
  z=malloc(N*sizeof(double));
  m=malloc(N*sizeof(double));
  myx=malloc(myN*sizeof(double));
  myy=malloc(myN*sizeof(double));
  myz=malloc(myN*sizeof(double));
  mym=malloc(myN*sizeof(double));
  Vx=malloc(myN*sizeof(double));
  Vy=malloc(myN*sizeof(double));
  Vz=malloc(myN*sizeof(double));
  Fx=malloc(myN*sizeof(double));
  Fy=malloc(myN*sizeof(double));
  Fz=malloc(myN*sizeof(double));
  disps=malloc(numprocs*sizeof(int));
  counts=malloc(numprocs*sizeof(int));

  /* Find out how many particles everyone has - the first 1 says we send on integer and the second says
     we receive 1 from each process - then create the displacements array 
     The displacement array will be used when we gather the coordinates */

  MPI_Allgather(&myN,1,MPI_INT,counts,1,MPI_INT,MPI_COMM_WORLD);

  disps[0]=0;
  for (j=1; j<numprocs; j++){
    disps[j]=counts[j-1]+disps[j-1];
  }

  /* Initialize - we assume the initial velocities are at t=-dt/2 */

  Initialize(myN,myid,myx,myy,myz,Vx,Vy,Vz,mym);

  /* Send the masses to everyone */

  MPI_Allgatherv(mym,myN,MPI_DOUBLE,m,counts,disps,MPI_DOUBLE,MPI_COMM_WORLD); 

  if (myid==0) {start_time=clock();}
  
  for (it=0; it<nsteps; it++){
    /* first the velocity step - to calculate forces we must send our positions */

    MPI_Allgatherv(myx,myN,MPI_DOUBLE,x,counts,disps,MPI_DOUBLE,MPI_COMM_WORLD);
    MPI_Allgatherv(myy,myN,MPI_DOUBLE,y,counts,disps,MPI_DOUBLE,MPI_COMM_WORLD);
    MPI_Allgatherv(myz,myN,MPI_DOUBLE,z,counts,disps,MPI_DOUBLE,MPI_COMM_WORLD);
    
    Force(myN,N,myx,myy,myz,x,y,z,m,Fx,Fy,Fz);
#pragma omp parallel for shared(Fx,Fy,Fz,Vx,Vy,Vz)
    for (j=0; j<myN; j++){
      Vx[j] += dt*Fx[j];
      Vy[j] += dt*Fy[j];
      Vz[j] += dt*Fz[j];
    }
  /* now position */
#pragma omp parallel for shared(myx,myy,myz,Vx,Vy,Vz)
    for (j=0; j<myN; j++){
      myx[j] += dt*Vx[j];
      myy[j] += dt*Vy[j];
      myz[j] += dt*Vz[j];
    }
  }

  if (myid==0){
    finish_time=clock();
    comp_time=((double) (finish_time-start_time))/CLOCKS_PER_SEC;
    printf("%.16e\n",comp_time);
  }
  
  /* Output the solution */

  /* Gather the final positions onto myid=0 */

  MPI_Gatherv(myx,myN,MPI_DOUBLE,x,counts,disps,MPI_DOUBLE,0,MPI_COMM_WORLD);
  MPI_Gatherv(myy,myN,MPI_DOUBLE,y,counts,disps,MPI_DOUBLE,0,MPI_COMM_WORLD);
  MPI_Gatherv(myz,myN,MPI_DOUBLE,z,counts,disps,MPI_DOUBLE,0,MPI_COMM_WORLD);
    
  if (myid==0){
    FID=fopen("Particle_positions10k_11","w");
    for (j=0; j<N; j++){
      fprintf(FID,"%.6e %.6e %.6e\n",x[j],y[j],z[j]);
    }
    fclose(FID);
  }
  MPI_Finalize();
}



