/* A program using Hypre to solve Laplace's equation in 3d 
   built off of examples 3 and 4 in the Hypre examples directory
   using their interface for structured grids */

#include <math.h>
#include "_hypre_utilities.h"
#include "HYPRE_krylov.h"
#include "HYPRE_struct_ls.h"

#ifdef M_PI
  #define PI M_PI
#else
  #define PI 3.14159265358979
#endif

/* Diffusion coefficient */
double sig(double x, double y, double z){
  return 10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.75)*(y-.75)-7.0*(z-.5)*(z-.5));
}

/* Right-hand side */
void Source(double* sloc, double x, double y, double z){
  double bg,bx,by,bz,sx,cx,sy,cy,sz,cz,kx,ky,kz;
  kx=2.0;
  ky=4.0;
  kz=3.0;
  cx=cos(PI*kx*x);
  sx=sin(PI*kx*x);
  cy=cos(PI*ky*y);
  sy=sin(PI*ky*y);
  cz=cos(PI*kz*z);
  sz=sin(PI*kz*z);
  bg=10.0*exp(-5.0*(x-.75)*(x-.75)-(y-.75)*(y-.75)-7.0*(z-.5)*(z-.5));
  bx=-10.0*(x-.75)*bg;
  by=-2.0*(y-.75)*bg;
  bz=-14.0*(z-.5)*bg;
  *sloc=bg*PI*PI*(kx*kx+ky*ky+kz*kz)*sx*sy*sz-bx*PI*kx*cx*sy*sz
    -by*PI*ky*sx*cy*sz-bz*PI*kz*sx*sy*cz;
}

void LaplaceSol(double* uloc,double x, double y, double z){
  double kx,ky,kz;
  kx=2.0;
  ky=4.0;
  kz=3.0;
  *uloc=sin(PI*kx*x)*sin(PI*ky*y)*sin(PI*kz*z);
}


int main (int argc, char *argv[])
{
   int i, j, k, ict;
   double err,errloc,size,sizeloc,uloc; 
   int myid, numprocs;

   int mmx, mmy, mmz, m, leftover, exsol;
   double dx,dy,dz,xloc,yloc,zloc,sloc;
   int ilower[3], iupper[3];

   int solver_id;
   int n_pre, n_post;
   int sym;
   int time_index;

   int num_iterations;
   double final_res_norm;

   HYPRE_StructGrid     grid;
   HYPRE_StructStencil  stencil;
   HYPRE_StructMatrix   A;
   HYPRE_StructVector   b;
   HYPRE_StructVector   x;
   HYPRE_StructSolver   solver;
   HYPRE_StructSolver   precond;

   MPI_Comm newcomm;
   int dims[3],mycoords[3],isperiodic[3],reorder;
   FILE* FID;
   
   /* Initialize MPI */
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &myid);
   MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

   exsol=1;
   
  isperiodic[0]=0;
  isperiodic[1]=0;
  isperiodic[2]=0;
  reorder=1;
  dims[0]=0;
  dims[1]=0;
  dims[2]=0; 
  MPI_Dims_create(numprocs,3,dims);
  MPI_Cart_create(MPI_COMM_WORLD,3,dims,isperiodic,reorder,&newcomm);

  /* Neighbors and coordinates */
 
  MPI_Comm_rank(newcomm,&myid);
  MPI_Cart_coords(newcomm,myid,3,mycoords); 
  

   /* Set parameters */
   m         = 400;
   solver_id = 10; /* PCG with multigrid */
   n_pre     = 1;  /* prerelaxations */
   n_post    = 1;  /* postrelaxations */
   sym       = 1;  /* symmetric storage mode */ 

  leftover=m%dims[0]; 
  mmx=m/dims[0];

  if (mmx < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[0] < leftover+1){ilower[0]=(mmx+1)*mycoords[0];}
  else {ilower[0]=(mmx+1)*leftover+mmx*(mycoords[0]-leftover);}

  if (mycoords[0] < leftover){mmx=mmx+1;}

  iupper[0]=ilower[0]+mmx-1; 

  leftover=m%dims[1]; 
  mmy=m/dims[1];

  if (mmy < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[1] < leftover+1){ilower[1]=(mmy+1)*mycoords[1];}
  else {ilower[1]=(mmy+1)*leftover+mmy*(mycoords[1]-leftover);}

  if (mycoords[1] < leftover){mmy=mmy+1;}

  iupper[1]=ilower[1]+mmy-1;
  
  leftover=m%dims[2]; 
  mmz=m/dims[2];

  if (mmz < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[2] < leftover+1){ilower[2]=(mmz+1)*mycoords[2];}
  else {ilower[2]=(mmz+1)*leftover+mmz*(mycoords[2]-leftover);}

  if (mycoords[2] < leftover){mmz=mmz+1;}

  iupper[2]=ilower[2]+mmz-1;

   /* Set up the mesh  */ 

  dx=1.0/((double) (m+1));
  dy=dx;
  dz=dx;

  /* Save basic input information - we do all io on myid=0 */

  if (myid==0){
    FID=fopen("Laplace_Hypre.case2","w");
    fprintf(FID, "m=%i\n",m);
    fprintf(FID, "numprocs=%i\n",numprocs);
    fprintf(FID, "Hypre preconditioner=%i\n",solver_id); 
  } 

  /* Set up a grid */
   {
      /* Create an empty 3D grid object */
      HYPRE_StructGridCreate(newcomm, 3, &grid);

      /* Add a new box to the grid */
      HYPRE_StructGridSetExtents(grid, ilower, iupper);

      /* This is a collective call finalizing the grid assembly.
         The grid is now ``ready to be used'' */
      HYPRE_StructGridAssemble(grid);
   }

   /* Define the discretization stencil using symmetric storage */
   {
      /* Define the geometry of the stencil */
     int offsets[4][3] = {{0,0,0}, {1,0,0}, {0,1,0},{0,0,1}}; 

      /* Create an empty 3D, 4-pt stencil object */
      HYPRE_StructStencilCreate(3, 4, &stencil);

      /* Assign stencil entries */
      for (i = 0; i < 4; i++)
         HYPRE_StructStencilSetElement(stencil, i, offsets[i]);
   }
   /* Set up a Struct Matrix */
   {
      int nentries = 4;
      int nvalues = nentries*mmx*mmy*mmz;
      double *values;
      int stencil_indices[4];

      /* Create an empty matrix object */
      HYPRE_StructMatrixCreate(newcomm, grid, stencil, &A);

      /* Use symmetric storage */
      HYPRE_StructMatrixSetSymmetric(A, sym);

      /* Indicate that the matrix coefficients are ready to be set */
      HYPRE_StructMatrixInitialize(A);

      values = (double*) calloc(nvalues, sizeof(double));

      for (j = 0; j < nentries; j++)
         stencil_indices[j] = j;

      /* Set the standard stencil at each grid point,
         we will fix the boundaries later */
      ict=0;
      for (k=0; k<mmz; k++){
	for (j=0; j<mmy; j++){
	  for (i=0; i<mmx; i++){
	     xloc=dx*(ilower[0]+i+1);
             yloc=dy*(ilower[1]+j+1);
             zloc=dz*(ilower[2]+k+1);
             values[ict+1]=-sig(xloc+0.5*dx,yloc,zloc)/(dx*dx);
             values[ict+2]=-sig(xloc,yloc+0.5*dy,zloc)/(dy*dy);
             values[ict+3]=-sig(xloc,yloc,zloc+0.5*dz)/(dz*dz); 
             values[ict]=(sig(xloc+0.5*dx,yloc,zloc)+sig(xloc-0.5*dx,yloc,zloc))/(dx*dx)+
                (sig(xloc,yloc+0.5*dy,zloc)+sig(xloc,yloc-0.5*dy,zloc))/(dy*dy)+
		(sig(xloc,yloc,zloc+0.5*dz)+sig(xloc,yloc,zloc-0.5*dz))/(dz*dz);
             ict += 4;
	  }
	}
      }
         HYPRE_StructMatrixSetBoxValues(A, ilower, iupper, nentries,
                                        stencil_indices, values);

         free(values);
   }

   /*    Incorporate the zero boundary conditions: go along each face of
         the domain and set the stencil entry that reaches to the boundary to
         zero - start with the faces with normal x - since we assume symmetry we 
         only need to deal with half the boundaries - start with the right */
   {
      int bc_ilower[3];
      int bc_iupper[3];
      int nentries = 1;
      int nvalues  = nentries*mmy*mmz; /*  number of stencil entries times the length
                                     of one side of my grid box */
      double *values;
      int stencil_indices[1];

      values = (double*) calloc(nvalues, sizeof(double));
      for (j = 0; j < nvalues; j++)
         values[j] = 0.0;

      /* check using mycoords if I am on the right */
      if (mycoords[0] == dims[0]-1)
      {
	bc_ilower[0]=iupper[0];
	bc_ilower[1]=ilower[1];
	bc_ilower[2]=ilower[2];
	bc_iupper[0]=iupper[0];
	bc_iupper[1]=iupper[1];
	bc_iupper[2]=iupper[2];
        stencil_indices[0] = 1;

        HYPRE_StructMatrixSetBoxValues(A, bc_ilower, bc_iupper, nentries,
                                        stencil_indices, values);
      }
      free(values);
   }
   /* Now the top */
   {
      double *values;
      int bc_ilower[3];
      int bc_iupper[3];
      int nentries = 1;
      int stencil_indices[1];

      int nvalues=nentries*mmx*mmz;
      values = (double*) calloc(nvalues, sizeof(double));
      for (j = 0; j < nvalues; j++)
         values[j] = 0.0;

      /* check using mycoords if I am on the top */
      if (mycoords[1] == dims[1]-1)
      {
	bc_ilower[0]=ilower[0];
	bc_ilower[1]=iupper[1];
	bc_ilower[2]=ilower[2];
	bc_iupper[0]=iupper[0];
	bc_iupper[1]=iupper[1];
	bc_iupper[2]=iupper[2];
        stencil_indices[0] = 2;

        HYPRE_StructMatrixSetBoxValues(A, bc_ilower, bc_iupper, nentries,
                                        stencil_indices, values);
      }
      free(values);
   }
   /* Now forward */
   {
      double *values;
      int bc_ilower[3];
      int bc_iupper[3];
      int nentries = 1;
      int stencil_indices[1];

      int nvalues=nentries*mmx*mmy;
      values = (double*) calloc(nvalues, sizeof(double));
      for (j = 0; j < nvalues; j++)
         values[j] = 0.0;

      /* check using mycoords if I am on the right */
      if (mycoords[2] == dims[2]-1)
      {
	bc_ilower[0]=ilower[0];
	bc_ilower[1]=ilower[1];
	bc_ilower[2]=iupper[2];
	bc_iupper[0]=iupper[0];
	bc_iupper[1]=iupper[1];
	bc_iupper[2]=iupper[2];
        stencil_indices[0] = 3;

        HYPRE_StructMatrixSetBoxValues(A, bc_ilower, bc_iupper, nentries,
                                        stencil_indices, values);
      }
      free(values);
   }

   /* This is a collective call finalizing the matrix assembly.
      The matrix is now ``ready to be used'' */
   HYPRE_StructMatrixAssemble(A);


   /* Set up Struct Vectors for b and x */
   {

      /* Create an empty vector object */
      HYPRE_StructVectorCreate(newcomm, grid, &b);
      HYPRE_StructVectorCreate(newcomm, grid, &x);

      /* Indicate that the vector coefficients are ready to be set */
      HYPRE_StructVectorInitialize(b);
      HYPRE_StructVectorInitialize(x);

      double *values;

      values = (double*) calloc((mmx*mmy*mmz), sizeof(double));

      /* Set the values of b */
      ict=0;
      for (k=0; k<mmz; k++){
	for (j=0; j<mmy; j++){
	  for (i=0; i<mmx; i++){
	     xloc=dx*(ilower[0]+i+1);
             yloc=dy*(ilower[1]+j+1);
             zloc=dz*(ilower[2]+k+1);
             Source(&sloc,xloc,yloc,zloc);
	     values[ict]=sloc; 
             ict += 1;
	  }
	}
      }

      HYPRE_StructVectorSetBoxValues(b, ilower, iupper, values);

      /* Set x = 0 */
      for (i = 0; i < (mmx*mmy*mmz); i ++)
         values[i] = 0.0;
      HYPRE_StructVectorSetBoxValues(x, ilower, iupper, values);

      free(values);

   }

   /* Finalize the vector assembly */
   HYPRE_StructVectorAssemble(b);
   HYPRE_StructVectorAssemble(x);

   /* Set up and use a solver */

   /* PCG parameters */

   time_index = hypre_InitializeTiming("PCG Setup");
   hypre_BeginTiming(time_index);

   HYPRE_StructPCGCreate(newcomm, &solver);
   HYPRE_StructPCGSetMaxIter(solver, 2000 );
   HYPRE_StructPCGSetTol(solver, 1.0e-06 );
   HYPRE_StructPCGSetTwoNorm(solver, 1 );
   HYPRE_StructPCGSetRelChange(solver, 0 );
   HYPRE_StructPCGSetPrintLevel(solver, 2 );

   /* use symmetric SMG as preconditioner */

   HYPRE_StructSMGCreate(newcomm, &precond);
   HYPRE_StructSMGSetMemoryUse(precond, 0);
   HYPRE_StructSMGSetMaxIter(precond, 1);
   HYPRE_StructSMGSetTol(precond, 0.0);
   HYPRE_StructSMGSetZeroGuess(precond);
   HYPRE_StructSMGSetNumPreRelax(precond, n_pre);
   HYPRE_StructSMGSetNumPostRelax(precond, n_post);
   HYPRE_StructSMGSetPrintLevel(precond, 0);
   HYPRE_StructSMGSetLogging(precond, 0);
   HYPRE_StructPCGSetPrecond(solver,HYPRE_StructSMGSolve,HYPRE_StructSMGSetup,precond);

   /* PCG Setup */
   HYPRE_StructPCGSetup(solver, A, b, x );

   hypre_EndTiming(time_index);
   hypre_PrintTiming("Setup phase times", newcomm);
   hypre_FinalizeTiming(time_index);
   hypre_ClearTiming();

   time_index = hypre_InitializeTiming("PCG Solve");
   hypre_BeginTiming(time_index);

   /* PCG Solve */
   HYPRE_StructPCGSolve(solver, A, b, x);

   hypre_EndTiming(time_index);
   hypre_PrintTiming("Solve phase times", newcomm);
   hypre_FinalizeTiming(time_index);
   hypre_ClearTiming();

   /* Get info and release memory */
   HYPRE_StructPCGGetNumIterations( solver, &num_iterations );
   HYPRE_StructPCGGetFinalRelativeResidualNorm( solver, &final_res_norm );
   HYPRE_StructPCGDestroy(solver);
   HYPRE_StructSMGDestroy(precond);
   if (myid == 0)
   {
      printf("\n");
      printf("Iterations = %d\n", num_iterations);
      printf("Final Relative Residual Norm = %e\n", final_res_norm);
      printf("\n");
   }
   if (exsol) {

     int nvalues = mmx*mmy*mmz;
      double *values =  (double*) calloc(nvalues, sizeof(double));

      /* get the local solution */
      HYPRE_StructVectorGetBoxValues(x, ilower, iupper, values);

     errloc=0.0;
     sizeloc=0.0;
     ict=0;
     for (k=0; k<mmz; k++) {
       for (j=0; j<mmy; j++) {
         for (i=0; i<mmx; i++){
           xloc=dx*(ilower[0]+i+1);
           yloc=dy*(ilower[1]+j+1);
           zloc=dz*(ilower[2]+k+1);
           LaplaceSol(&uloc,xloc,yloc,zloc);
           errloc += dx*dy*dz*(values[ict]-uloc)*(values[ict]-uloc);
           sizeloc += dx*dy*dz*uloc*uloc;
	   ict += 1; 
        }
      }
    }
     free(values);
    MPI_Reduce(&errloc,&err,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
    MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
    if (myid == 0) fprintf(FID,"Final relative error %.16e\n",sqrt(err/size));
   }

   if (myid ==0) fclose(FID);


   /* Free memory */
   HYPRE_StructGridDestroy(grid);
   HYPRE_StructStencilDestroy(stencil);
   HYPRE_StructMatrixDestroy(A);
   HYPRE_StructVectorDestroy(b);
   HYPRE_StructVectorDestroy(x);

   /* Finalize MPI */
   MPI_Finalize();

   return (0);
}

