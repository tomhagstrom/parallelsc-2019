/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include "mpi.h"
#include "Laplace_3d.h"

void Preco(double q[], double r[], int mmx, int mmy, int mmz, double x[], double y[], double z[],
	   double dx, double dy, double dz); 

/* Use preconditioned conjugate gradients to solve
   -div(sigma grad u)=f with zero bcs on the unit cube */

int main(int argc, char* argv[]) {

  /* declarations */

  int m,jout,j,k,i,itmax,it,exsol;
  int myid,numprocs,myup,mydown,myleft,myright,mybck,myfor,myzb,myxl,myyd,mmx,mmy,mmz,leftover;  
  double *u,*w,*p,*r,*q,*x,*y,*z,*us,*ur,*ds,*dr,*ls,*lr,*rs,*rr,*bs,*br,*fs,*fr;
  double dx,dy,dz,err,size,errloc,sizeloc,qtrold,ptw,ptwloc,qtr,qtrloc,mu,nu,uloc,ctol;
  double sigx[2],sigy[2],sigz[2];
  double comptime,communtime,iotime,tc1,tc2,ti1,ti2,tt1,tt2; 
  MPI_Request request[12];
  MPI_Status statr[12];
  MPI_Comm newcomm;
  int dims[3],mycoords[3],isperiodic[3],reorder,ierr;
  FILE* FID; 
  
  /* set parameters */

  m=200;
  exsol=1; /* This is a logical variable =0 -> No exact solution available */ 
  ctol=1.0E-6;
  itmax=50000;
  

  /*  Set up the communicator */
 
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  /* Now use the MPI Cartesian functions to decompose the domain */

  isperiodic[0]=0;
  isperiodic[1]=0;
  isperiodic[2]=0;
  reorder=1;
  dims[0]=0;
  dims[1]=0;
  dims[2]=0; 
  MPI_Dims_create(numprocs,3,dims);
  MPI_Cart_create(MPI_COMM_WORLD,3,dims,isperiodic,reorder,&newcomm);

  /* Neighbors and coordinates */
 
  MPI_Comm_rank(newcomm,&myid);
  MPI_Cart_shift(newcomm,0,1,&myleft,&myright);
  MPI_Cart_shift(newcomm,1,1,&mydown,&myup);
  MPI_Cart_shift(newcomm,2,1,&mybck,&myfor);
  MPI_Cart_coords(newcomm,myid,3,mycoords); 
  
  /* Now we divvy out the data - I will improve on what I did in the first code
     which could cause the last processor to be overloaded - here I will determine
     left and right indices and up and down indices for each process */

  leftover=m%dims[0]; 
  mmx=m/dims[0];

  if (mmx < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[0] < leftover+1){myxl=(mmx+1)*mycoords[0]+1;}
  else {myxl=(mmx+1)*leftover+mmx*(mycoords[0]-leftover)+1;}

  if (mycoords[0] < leftover){mmx=mmx+1;}

  leftover=m%dims[1]; 
  mmy=m/dims[1];

  if (mmy < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[1] < leftover+1){myyd=(mmy+1)*mycoords[1]+1;}
  else {myyd=(mmy+1)*leftover+mmy*(mycoords[1]-leftover)+1;}

  if (mycoords[1] < leftover){mmy=mmy+1;}

  leftover=m%dims[2]; 
  mmz=m/dims[2];

  if (mmz < 3){printf("Too many processors for the problem size"); exit(1);}

  if (mycoords[2] < leftover+1){myzb=(mmz+1)*mycoords[2]+1;}
  else {myzb=(mmz+1)*leftover+mmz*(mycoords[2]-leftover)+1;}

  if (mycoords[2] < leftover){mmz=mmz+1;}

  u=malloc(mmx*mmy*mmz*sizeof(double));
  w=malloc(mmx*mmy*mmz*sizeof(double));
  r=malloc(mmx*mmy*mmz*sizeof(double));
  q=malloc(mmx*mmy*mmz*sizeof(double));
  p=malloc((mmx+2)*(mmy+2)*(mmz+2)*sizeof(double));
  x=malloc(mmx*sizeof(double));
  y=malloc(mmy*sizeof(double));
  z=malloc(mmz*sizeof(double));
  us=malloc(mmx*mmz*sizeof(double));
  ur=malloc(mmx*mmz*sizeof(double));
  ds=malloc(mmx*mmz*sizeof(double));
  dr=malloc(mmx*mmz*sizeof(double));
  rs=malloc(mmy*mmz*sizeof(double));
  rr=malloc(mmy*mmz*sizeof(double));
  ls=malloc(mmy*mmz*sizeof(double));
  lr=malloc(mmy*mmz*sizeof(double));
  fs=malloc(mmx*mmy*sizeof(double));
  fr=malloc(mmx*mmy*sizeof(double));
  bs=malloc(mmx*mmy*sizeof(double));
  br=malloc(mmx*mmy*sizeof(double));

  /* Set up the mesh  */ 

  dx=1.0/((double) (m+1));
  dy=dx;
  dz=dx;


  for (i=0; i<mmx; i++) x[i]=(i+myxl)*dx;

  for (j=0; j<mmy; j++) y[j]=(j+myyd)*dy;

  for (k=0; k<mmz; k++) z[k]=(k+myzb)*dy;

  /* Save basic input information - we do all io on myid=0 */

  if (myid==0){
    FID=fopen("Laplace_PCG.case1","w");
    fprintf(FID, "m=%i\n",m);
    fprintf(FID, "Jacobi preconditioner\n"); 
  } 

  /* Initialize: the initial guess for u is zero so the initial residual is
     simply the source function */

  for (k=0; k<mmz; k++) {
    for (j=0; j<mmy; j++) {
      for (i=0; i<mmx; i++) {
	Source(&r[id3d(i,j,k,mmx,mmy,mmz)],x[i],y[j],z[k]);
	u[id3d(i,j,k,mmx,mmy,mmz)]=0.0;
	w[id3d(i,j,k,mmx,mmy,mmz)]=0.0;
      }
    }
  }

  Preco(q,r,mmx,mmy,mmz,x,y,z,dx,dy,dz); 
  
  for (k=0; k<mmz; k++) {
    for (j=0; j<mmy; j++) {
      for (i=0; i<mmx; i++) {
	p[id3dg(i,j,k,mmx,mmy,mmz)]=q[id3d(i,j,k,mmx,mmy,mmz)];
      }
    }
  }

  for (i=-1; i<mmx+1; i++){
    for (j=-1; j<mmy+1; j++){
      p[id3dg(i,j,-1,mmx,mmy,mmz)]=0.0;
      p[id3dg(i,j,mmz,mmx,mmy,mmz)]=0.0;
    }
    for (k=0; k<mmz; k++){
      p[id3dg(i,-1,k,mmx,mmy,mmz)]=0.0;
      p[id3dg(i,mmy,k,mmx,mmy,mmz)]=0.0;
    }
  }
  for (k=0; k<mmz; k++){
    for (j=0; j<mmy; j++){
      p[id3dg(-1,j,k,mmx,mmy,mmz)]=0.0;
      p[id3dg(mmx,j,k,mmx,mmy,mmz)]=0.0;
    }
  }
      
  /* Now iterate - start timers */

  comptime=0.0;
  iotime=0.0;
  communtime=0.0; 

  tc1=MPI_Wtime();

  sizeloc=0.0;
  for (k=0; k<mmz; k++){
    for (j=0; j<mmy; j++){
      for (i=0; i<mmx; i++){
	sizeloc += r[id3d(i,j,k,mmx,mmy,mmz)]*r[id3d(i,j,k,mmx,mmy,mmz)];
      }
    }
  }

  qtrloc=0.0;
  for (k=0; k<mmz; k++){
    for (j=0; j<mmy; j++){
      for (i=0; i<mmx; i++){
	qtrloc += r[id3d(i,j,k,mmx,mmy,mmz)]*q[id3d(i,j,k,mmx,mmy,mmz)];
      }
    }
  }

  tc2=MPI_Wtime();
  comptime += tc2-tc1;

  tt1=MPI_Wtime();
  
  MPI_Allreduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,newcomm);

  MPI_Allreduce(&qtrloc,&qtr,1,MPI_DOUBLE,MPI_SUM,newcomm);

  tt2=MPI_Wtime();
  communtime += tt2-tt1;

  it=1;
  
  ti1=MPI_Wtime();

  if (myid == 0) fprintf(FID,"%i %.16e\n",it,sqrt(size)); 

  ti2=MPI_Wtime();
  iotime += ti2-ti1;

  while ( (it <= itmax) && (sqrt(size) > ctol) ){

    tt1=MPI_Wtime();

    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	rs[id2d(j,k,mmy,mmz)]=p[id3dg(mmx-1,j,k,mmx,mmy,mmz)];
	ls[id2d(j,k,mmy,mmz)]=p[id3dg(0,j,k,mmx,mmy,mmz)];
	rr[id2d(j,k,mmy,mmz)]=0.0;
	lr[id2d(j,k,mmy,mmz)]=0.0;
      }
    }
	
    for (k=0; k<mmz; k++){
      for (i=0; i<mmx; i++){
	us[id2d(i,k,mmx,mmz)]=p[id3dg(i,mmy-1,k,mmx,mmy,mmz)];
	ds[id2d(i,k,mmx,mmz)]=p[id3dg(i,0,k,mmx,mmy,mmz)];
	ur[id2d(i,k,mmx,mmz)]=0.0;
	dr[id2d(i,k,mmx,mmz)]=0.0;
      }
    }
	
    for (j=0; j<mmy; j++){
      for (i=0; i<mmx; i++){
	fs[id2d(i,j,mmx,mmy)]=p[id3dg(i,j,mmz-1,mmx,mmy,mmz)];
	bs[id2d(i,j,mmx,mmy)]=p[id3dg(i,j,0,mmx,mmy,mmz)];
	fr[id2d(i,j,mmx,mmy)]=0.0;
	br[id2d(i,j,mmx,mmy)]=0.0;
      }
    }
	
    MPI_Irecv(dr,mmx*mmz,MPI_DOUBLE,mydown,0,newcomm,&request[0]);
    MPI_Irecv(ur,mmx*mmz,MPI_DOUBLE,myup,1,newcomm,&request[1]);
    MPI_Irecv(lr,mmy*mmz,MPI_DOUBLE,myleft,2,newcomm,&request[2]);
    MPI_Irecv(rr,mmy*mmz,MPI_DOUBLE,myright,3,newcomm,&request[3]);
    MPI_Irecv(br,mmx*mmy,MPI_DOUBLE,mybck,4,newcomm,&request[4]);
    MPI_Irecv(fr,mmx*mmy,MPI_DOUBLE,myfor,5,newcomm,&request[5]);
    MPI_Isend(us,mmx*mmz,MPI_DOUBLE,myup,0,newcomm,&request[6]);
    MPI_Isend(ds,mmx*mmz,MPI_DOUBLE,mydown,1,newcomm,&request[7]);
    MPI_Isend(rs,mmy*mmz,MPI_DOUBLE,myright,2,newcomm,&request[8]);
    MPI_Isend(ls,mmy*mmz,MPI_DOUBLE,myleft,3,newcomm,&request[9]);
    MPI_Isend(fs,mmx*mmy,MPI_DOUBLE,myfor,4,newcomm,&request[10]);
    MPI_Isend(bs,mmx*mmy,MPI_DOUBLE,mybck,5,newcomm,&request[11]);

    /* Now we must wait for the communications to complete */

    MPI_Waitall(12,request,statr);

    if (mycoords[0] < (dims[0]-1)){
      for (k=0; k<mmz; k++){
	for (j=0; j<mmy; j++){
	  p[id3dg(mmx,j,k,mmx,mmy,mmz)]=rr[id2d(j,k,mmy,mmz)];
	}
      }
    }

    if (mycoords[0] > 0){
      for (k=0; k<mmz; k++){
	for (j=0; j<mmy; j++){
	  p[id3dg(-1,j,k,mmx,mmy,mmz)]=lr[id2d(j,k,mmy,mmz)];
	}
      }
    }

    if (mycoords[1] < (dims[1]-1)){
      for (k=0; k<mmz; k++){
	for (i=0; i<mmx; i++){
	  p[id3dg(i,mmy,k,mmx,mmy,mmz)]=ur[id2d(i,k,mmx,mmz)];
	}
      }
    }

    if (mycoords[1] > 0){
      for (k=0; k<mmz; k++){
	for (i=0; i<mmx; i++){
	  p[id3dg(i,-1,k,mmx,mmy,mmz)]=dr[id2d(i,k,mmx,mmz)];
	}
      }
    }

    if (mycoords[2] < (dims[2]-1)){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  p[id3dg(i,j,mmz,mmx,mmy,mmz)]=fr[id2d(i,j,mmx,mmy)];
	}
      }
    }

    if (mycoords[2] > 0){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  p[id3dg(i,j,-1,mmx,mmy,mmz)]=br[id2d(i,j,mmx,mmy)];
	}
      }
    }

    tt2=MPI_Wtime();
    communtime += tt2-tt1;

    tc1=MPI_Wtime();

    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  diffun(sigx,sigy,sigz,x[i],y[j],z[k],dx,dy,dz);
	  w[id3d(i,j,k,mmx,mmy,mmz)] = -(sigx[1]*(p[id3dg(i+1,j,k,mmx,mmy,mmz)]-p[id3dg(i,j,k,mmx,mmy,mmz)])-
				         sigx[0]*(p[id3dg(i,j,k,mmx,mmy,mmz)]-p[id3dg(i-1,j,k,mmx,mmy,mmz)]))/(dx*dx);
	  w[id3d(i,j,k,mmx,mmy,mmz)] -= (sigy[1]*(p[id3dg(i,j+1,k,mmx,mmy,mmz)]-p[id3dg(i,j,k,mmx,mmy,mmz)])-
				         sigy[0]*(p[id3dg(i,j,k,mmx,mmy,mmz)]-p[id3dg(i,j-1,k,mmx,mmy,mmz)]))/(dy*dy);
	  w[id3d(i,j,k,mmx,mmy,mmz)] -= (sigz[1]*(p[id3dg(i,j,k+1,mmx,mmy,mmz)]-p[id3dg(i,j,k,mmx,mmy,mmz)])-
				         sigz[0]*(p[id3dg(i,j,k,mmx,mmy,mmz)]-p[id3dg(i,j,k-1,mmx,mmy,mmz)]))/(dz*dz);
	}
      }
    }

    ptwloc=0.0;


    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  ptwloc += p[id3dg(i,j,k,mmx,mmy,mmz)]*w[id3d(i,j,k,mmx,mmy,mmz)];
	}
      }
    }

    tc2=MPI_Wtime();
    comptime += tc2-tc1; 

    tt1=MPI_Wtime();

    MPI_Allreduce(&ptwloc,&ptw,1,MPI_DOUBLE,MPI_SUM,newcomm);

    tt2=MPI_Wtime();
    communtime += tt2-tt1;

    tc1=MPI_Wtime();

    nu=qtr/ptw;

    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  u[id3d(i,j,k,mmx,mmy,mmz)] += nu*p[id3dg(i,j,k,mmx,mmy,mmz)];
	}
      }
    }
    
    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  r[id3d(i,j,k,mmx,mmy,mmz)] -= nu*w[id3d(i,j,k,mmx,mmy,mmz)];
	}
      }
    }

    Preco(q,r,mmx,mmy,mmz,x,y,z,dx,dy,dz); 
  
    qtrold=qtr;
    qtrloc=0.0;
    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  qtrloc += r[id3d(i,j,k,mmx,mmy,mmz)]*q[id3d(i,j,k,mmx,mmy,mmz)];
	}
      }
    }

    sizeloc=0.0;
    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  sizeloc += r[id3d(i,j,k,mmx,mmy,mmz)]*r[id3d(i,j,k,mmx,mmy,mmz)];
	}
      }
    }

    tc2=MPI_Wtime();
    comptime += tc2-tc1; 

    tt1=MPI_Wtime();

    MPI_Allreduce(&qtrloc,&qtr,1,MPI_DOUBLE,MPI_SUM,newcomm);

    MPI_Allreduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,newcomm);

    tt2=MPI_Wtime();
    communtime += tt2-tt1;

    tc1=MPI_Wtime();

    mu=qtr/qtrold;

    for (k=0; k<mmz; k++){
      for (j=0; j<mmy; j++){
	for (i=0; i<mmx; i++){
	  p[id3dg(i,j,k,mmx,mmy,mmz)] = q[id3d(i,j,k,mmx,mmy,mmz)]+mu*p[id3dg(i,j,k,mmx,mmy,mmz)];
	}
      }
    }

    it += 1;
    
    tc2=MPI_Wtime();
    comptime += tc2-tc1; 

    ti1=MPI_Wtime();

    if (myid == 0) fprintf(FID,"%i %.16e\n",it,sqrt(size)); 

    ti2=MPI_Wtime();
    iotime += ti2-ti1;
  }

  if (exsol) {
    errloc=0.0;
    sizeloc=0.0;
    for (k=0; k<mmz; k++) {
      for (j=0; j<mmy; j++) {
	for (i=0; i<mmx; i++){
	  LaplaceSol(&uloc,x[i],y[j],z[k]);
	  errloc += dx*dy*dz*(u[id3d(i,j,k,mmx,mmy,mmz)]-uloc)*(u[id3d(i,j,k,mmx,mmy,mmz)]-uloc);
          sizeloc += dx*dy*dz*uloc*uloc;
	}
      }
    }
    MPI_Reduce(&errloc,&err,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
    MPI_Reduce(&sizeloc,&size,1,MPI_DOUBLE,MPI_SUM,0,newcomm);
    if (myid == 0) fprintf(FID,"Final relative error %.16e\n",sqrt(err/size)); 
  }
  if (myid == 0){
    fclose(FID);
    printf("Computation time=%.16e\n",comptime);
    printf("IO time=%.16e\n",iotime);
    printf("Communication time=%.16e\n",communtime);
  }
  MPI_Finalize();
}

void Preco(double q[], double r[], int mmx, int mmy, int mmz, double x[], double y[], double z[],
	   double dx, double dy, double dz){
  /* Apply Jacobi - the easiest but not the best */
  double del,sigx[2],sigy[2],sigz[2];
  int relax,i,j,k;
  relax=1; /* set to zero to do nothing */  
  for (k=0; k<mmz; k++){
    for (j=0; j<mmy; j++){
      for (i=0; i<mmx; i++){
	if (relax == 0) q[id3d(i,j,k,mmx,mmy,mmz)]=r[id3d(i,j,k,mmx,mmy,mmz)];
        else { diffun(sigx,sigy,sigz,x[i],y[j],z[k],dx,dy,dz);
               q[id3d(i,j,k,mmx,mmy,mmz)]=r[id3d(i,j,k,mmx,mmy,mmz)]/
               ((sigx[0]+sigx[1])/(dx*dx)+(sigy[0]+sigy[1])/(dy*dy)+(sigz[0]+sigz[1])/(dz*dz));
	}
      }
    }
  }
}    
