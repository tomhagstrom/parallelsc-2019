/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include "shallow_water.h"

/* Use a leap-frog method to solve the linearized shallow-water
   equations */  

int main(int argc, char* argv[]) {

  /* declarations */

  int m,jout,jwrite,j,k,nsteps,it,jdat,exsol;
  double *h,*u,*v,*hold,*uold,*vold,*x,*y;
  double ttot,c,cfl,dx,dt,t,err,size,lam,lamcs,dtr,swap,kx,ky,uloc,hloc,vloc;
  clock_t start_time,finish_time;
  double comp_time; 
  FILE* FW;
  FILE* FID; 
  
  /* set parameters */

  m=200;
  ttot=10.0;
  c=1.0;
  cfl=0.5;
  kx=1.0;
  ky=2.0; 
  jout=10;
  jwrite=1;
  exsol=1; /* This is a logical variable =0 -> No exact solution available */ 

  /* Allocate arrays - one-dimensional but an index function is used to simplify */ 

  h=malloc((m+2)*(m+2)*sizeof(double));
  u=malloc((m+2)*(m+2)*sizeof(double));
  v=malloc((m+2)*(m+2)*sizeof(double));
  hold=malloc(m*m*sizeof(double));
  uold=malloc(m*m*sizeof(double));
  vold=malloc(m*m*sizeof(double));
  x=malloc(m*sizeof(double));
  y=malloc(m*sizeof(double)); 

  /* Set up the mesh and time step */ 

  dx=1.0/m;
  dt=cfl*dx/c; 
  nsteps=(ttot/dt);
  dtr=ttot/nsteps;
  if (dtr > dt) {nsteps=nsteps+1; dt=ttot/nsteps;}
  else dt=dtr;

  lam=dt/dx; 
  lamcs=lam*c*c;

  for (j=0; j<m; j++) {
    x[j]=(j+1)*dx;
    y[j]=(j+1)*dx; }

  /* Save basic input information */

  FID=fopen("case1.txt","w");
  fprintf(FID, "m=%i\n",m);
  fprintf(FID, "nsteps=%i\n",nsteps);
  fprintf(FID, "ttot=%.16e\n",ttot);
  fprintf(FID, "c=%.16e\n",c);
  fprintf(FID, "kx=%.16e\n",kx);
  fprintf(FID, "ky=%.16e\n",ky);
  fclose(FID);

  /* Save x and y in a file for plotting */

  FID=fopen("x.txt","w");
  for (j=0; j<m; j++) fprintf(FID, "%.16e\n",x[j]);
  fclose(FID);

  FID=fopen("y.txt","w");
  for (k=0; k<m; k++) fprintf(FID, "%.16e\n",y[k]);
  fclose(FID);

  /* Open output file */

  FID=fopen("sw_error_data.case1","w"); 

  /* Initial data - note we need it at 2 time levels to get going
     but we assume that is taken care of in the ShallowWaterSol routine */

  for (k=0; k<m; k++) {
    for (j=0; j<m; j++) {
      ShallowWaterSol(&h[id2p(j,k,m)],&u[id2p(j,k,m)],&v[id2p(j,k,m)],x[j],y[k],0.0,kx,ky,c);
    }
  }

  /* Extend by periodicity */

  for (j=0; j<m; j++) {
    h[id2p(j,-1,m)]=h[id2p(j,m-1,m)];
    u[id2p(j,-1,m)]=u[id2p(j,m-1,m)];
    v[id2p(j,-1,m)]=v[id2p(j,m-1,m)];
    h[id2p(j,m,m)]=h[id2p(j,0,m)];
    u[id2p(j,m,m)]=u[id2p(j,0,m)];
    v[id2p(j,m,m)]=v[id2p(j,0,m)];
  }

  for (k=-1; k<m+1; k++) {
    h[id2p(-1,k,m)]=h[id2p(m-1,k,m)];
    u[id2p(-1,k,m)]=u[id2p(m-1,k,m)];
    v[id2p(-1,k,m)]=v[id2p(m-1,k,m)];
    h[id2p(m,k,m)]=h[id2p(0,k,m)];
    u[id2p(m,k,m)]=u[id2p(0,k,m)];
    v[id2p(m,k,m)]=v[id2p(0,k,m)];
  }

  for (k=0; k<m; k++) {
    for (j=0; j<m; j++) {
      ShallowWaterSol(&hold[id2(j,k,m)],&uold[id2(j,k,m)],&vold[id2(j,k,m)],x[j],y[k],-dt,kx,ky,c);
    }
  }

  /* Now march */

  comp_time=0.0; 

  for (it=0; it < nsteps; it++) {
    start_time=clock();
    for (k=0; k < m; k++) {
      for (j=0; j < m; j++) {
	hold[id2(j,k,m)] -= lam*(u[id2p(j+1,k,m)]-u[id2p(j-1,k,m)]+v[id2p(j,k+1,m)]-v[id2p(j,k-1,m)]);
	uold[id2(j,k,m)] -= lamcs*(h[id2p(j+1,k,m)]-h[id2p(j-1,k,m)]);
	vold[id2(j,k,m)] -= lamcs*(h[id2p(j,k+1,m)]-h[id2p(j,k-1,m)]); 
      } 
    } 

  /* Swap and extend */

    for (k=0; k < m; k++) {
      for (j=0; j < m; j++) {
	swap=hold[id2(j,k,m)];
	hold[id2(j,k,m)]=h[id2p(j,k,m)];
	h[id2p(j,k,m)]=swap;
	swap=uold[id2(j,k,m)];
	uold[id2(j,k,m)]=u[id2p(j,k,m)];
	u[id2p(j,k,m)]=swap;
	swap=vold[id2(j,k,m)];
	vold[id2(j,k,m)]=v[id2p(j,k,m)];
	v[id2p(j,k,m)]=swap;
      }
      h[id2p(-1,k,m)]=h[id2p(m-1,k,m)];
      u[id2p(-1,k,m)]=u[id2p(m-1,k,m)];
      v[id2p(-1,k,m)]=v[id2p(m-1,k,m)];
      h[id2p(m,k,m)]=h[id2p(0,k,m)];
      u[id2p(m,k,m)]=u[id2p(0,k,m)];
      v[id2p(m,k,m)]=v[id2p(0,k,m)];
    }
    for (j=-1; j < m+1; j++) {
      h[id2p(j,-1,m)]=h[id2p(j,m-1,m)];
      u[id2p(j,-1,m)]=u[id2p(j,m-1,m)];
      v[id2p(j,-1,m)]=v[id2p(j,m-1,m)];
      h[id2p(j,m,m)]=h[id2p(j,0,m)];
      u[id2p(j,m,m)]=u[id2p(j,0,m)];
      v[id2p(j,m,m)]=v[id2p(j,0,m)];
    }

    finish_time=clock();
    comp_time += ((double) (finish_time-start_time))/CLOCKS_PER_SEC; 
    /* Check for output and saving */ 

    if (((it+1)%jout)==0) {
      t=(it+1)*dt;
      if (exsol) {
	err=0.0;
	size=0.0;
	for (k=0; k<m; k++) {
	  for (j=0; j<m; j++) {
	    ShallowWaterSol(&hloc,&uloc,&vloc,x[j],y[k],t,kx,ky,c);
	    err += dx*dx*(c*c*(h[id2p(j,k,m)]-hloc)*(h[id2p(j,k,m)]-hloc)
			  +(u[id2p(j,k,m)]-uloc)*(u[id2p(j,k,m)]-uloc)+(v[id2p(j,k,m)]-vloc)*(v[id2p(j,k,m)]-vloc));
	    size += dx*dx*(c*c*hloc*hloc+uloc*uloc+vloc*vloc);
	  }
	}
	fprintf(FID,"%.16e %.16e\n",t,sqrt(err/size));  /* Relative error */
      }
      else {
	size=0.0;
	for (k=0; k<m; k++) {
	  for (j=0; j<m; j++) {
	    size += dx*dx*(c*c*h[id2p(j,k,m)]*h[id2p(j,k,m)]+u[id2p(j,k,m)]*u[id2p(j,k,m)]+v[id2p(j,k,m)]*v[id2p(j,k,m)]);
	  }
	}
	fprintf(FID,"%.16e %.16e\n",t,sqrt(size)); 
      }
    }

    if ((jwrite*(it+1)%nsteps)==0) {
      jdat=jwrite*it/nsteps;
      FW=fopen("swsoldata_case1.01","w");
      for (k=0; k<m; k++) {
	for (j=0; j<m; j++) {
	  fprintf(FW,"%.16e %.16e %.16e\n",h[id2p(j,k,m)],u[id2p(j,k,m)],v[id2p(j,k,m)]);
	}
      }
      fclose(FW);
    }
  }
  fclose(FID);
  printf("Computing time %.6e\n",comp_time);

}


