/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h> 
#include <cufft.h>
#include <cuComplex.h> 

#define BATCH 1

void f(double *y, double x){
  double pi=3.1415926535897932385;
  *y=exp(-20.0*cos(2.0*pi*x)*cos(2.0*pi*x));
}

void df(double *y, double x){
  double pi=3.1415926535897932385;
  *y=80.0*pi*cos(2.0*pi*x)*sin(2.0*pi*x)*exp(-20.0*cos(2.0*pi*x)*cos(2.0*pi*x));
}

__global__ void FDiff(cufftDoubleComplex *fhat, cufftDoubleComplex *dfhat, int N, double perf) {

  int j = blockIdx.x * blockDim.x + threadIdx.x;

  if (j < (N/2+1)){
      dfhat[j]=cuCdiv(cuCmul(fhat[j],make_cuDoubleComplex(0.0,perf*((double) j))),make_cuDoubleComplex((double) N,0.0));
   }
}


int main(int argc, char **argv) {
cufftHandle planf,plani;
cufftDoubleComplex *fhat,*dfhat;
cufftDoubleReal *fdat,*fder;
double *fder_host,*fdat_host;  
double x,err,errloc,y,perf;
double pi=3.1415926535897932385;
int N,q,j;

perf=2.0*pi; 

for (q=3; q<8; q++){
  N=pow(2,q);
  cudaMalloc((void**)&fhat, sizeof(cufftDoubleComplex)*(N/2+1)*BATCH);
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  cudaMalloc((void**)&dfhat, sizeof(cufftDoubleComplex)*(N/2+1)*BATCH);
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  cudaMalloc((void**)&fdat, sizeof(cufftDoubleReal)*N*BATCH);
  if (cudaGetLastError() != cudaSuccess){
	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  cudaMalloc((void**)&fder, sizeof(cufftDoubleReal)*N*BATCH);
  if (cudaGetLastError() != cudaSuccess){
	fprintf(stderr, "Cuda error: Failed to allocate\n");
	return (-1);	
  }
  if (cufftPlan1d(&planf, N, CUFFT_D2Z, BATCH) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: Plan creation failed");
	return (-1);	
  }
  if (cufftPlan1d(&plani, N, CUFFT_Z2D, BATCH) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: Plan creation failed");
	return (-1);	
  }
  fdat_host=(double *) malloc(N*sizeof(double));
  fder_host=(double *) malloc(N*sizeof(double));
  for (j=0; j<N; j++){
    x=((double) j)/((double) N);
    f(&y,x);
    fdat_host[j]=y;
    }
  cudaMemcpy(fdat,fdat_host,N*sizeof(double),cudaMemcpyHostToDevice); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to copy\n");
	return (-1);	
  }

/* Forward transform */

  if (cufftExecD2Z(planf, fdat, fhat) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: ExecD2Z Forward failed");
	return (-1);	
  }
  
  if (cudaDeviceSynchronize() != cudaSuccess){
    	fprintf(stderr, "Cuda error: Failed to synchronize\n");
	return (-1);	
  }

/* Differentiate the data and normalize */

  FDiff<<<1,(N/2)+1>>>(fhat,dfhat,N,perf); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to differentiate\n");
	return (-1);	
  }

  if (cudaDeviceSynchronize() != cudaSuccess){
    	fprintf(stderr, "Cuda error: Failed to synchronize\n");
	return (-1);	
  }

/* Inverse transform */

  if (cufftExecZ2D(plani, dfhat, fder) != CUFFT_SUCCESS){
	fprintf(stderr, "CUFFT error: ExecZ2D Inverse failed");
	return (-1);	
  }

  cudaMemcpy(fder_host,fder,N*sizeof(double),cudaMemcpyDeviceToHost); 
  if (cudaGetLastError() != cudaSuccess){
  	fprintf(stderr, "Cuda error: Failed to copy\n");
	return (-1);	
  }

/* Check error */

  err=0.0;
  for (j=0; j<N; j++){
    x=((double) j)/((double) N);
    df(&y,x);
    errloc=fabs(y-fder_host[j]);
    if (errloc > err) err=errloc;
    }
  printf("N=%i err=%.4e\n",N,err); 

  cufftDestroy(planf);
  cufftDestroy(plani);
  cudaFree(fhat);
  cudaFree(dfhat);
  cudaFree(fdat);
  cudaFree(fder);
  free(fdat_host); 
  free(fder_host); 
}

return 0;

}


