/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h> 
#include <omp.h> 

/* Solve for the motion of N bodies using Newton's laws */

void Force(int N, double x[], double y[], double z[], double m[], double Fx[], double Fy[], double Fz[]){
  /* Calculate forces by Newton's Laws */
  double dx,dy,dz,rad,ris,small;
  int j,k;
  small=1e-20;  /* to avoid ifs in loops */ 
#pragma omp parallel for private(dx,dy,dz,rad,k,ris) shared(x,y,z,m,Fx,Fy,Fz)
  for (j=0; j<N; j++){
    Fx[j]=0.0;
    Fy[j]=0.0;
    Fz[j]=0.0;
    for (k=0; k<N; k++){
      dx=x[k]-x[j];
      dy=y[k]-y[j];
      dz=z[k]-z[j];
      rad=sqrt(dx*dx+dy*dy+dz*dz+small);
      ris=m[k]/(rad*rad*rad);
      Fx[j] += dx*ris;
      Fy[j] += dy*ris;
      Fz[j] += dz*ris;
    }
  }
}

void Initialize(int N, double x[], double y[], double z[], double Vx[], double Vy[], double Vz[], double m[]){
  /* Initial positions, velocities, and masses */
    int j,rnum; /* Note - this is not a good random number generator but I don't care if these values are truly random */
    double rad,theta,phi;
    double pi=3.14159265358979;
    double mbar=5.0;  /* average mass */
    double Vbar=3.0;  /* mean initial outward velocity */
    double R=5.0; /* masses initially in a sphere of radius R */
#pragma omp parallel for private(rnum,rad,theta,phi) shared(x,y,z,Vx,Vy,Vz,m)
  for (j=0; j<N; j++){
    rnum=rand();
    rad=5.0*((double) (rnum))/((double) (RAND_MAX));
    rnum=rand();
    theta=pi*((double) (rnum))/((double) (RAND_MAX));
    rnum=rand();
    phi=2.0*pi*((double) (rnum))/((double) (RAND_MAX));
    x[j]=rad*sin(theta)*cos(phi);
    y[j]=rad*sin(theta)*sin(phi);
    z[j]=rad*cos(theta); 
    rnum=rand();
    Vx[j]=Vbar*x[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    Vy[j]=Vbar*y[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    Vz[j]=Vbar*z[j]+(6.0*((double) (rnum))/((double) (RAND_MAX))-3.0);
    rnum=rand();
    m[j]=mbar+(8.0*((double) (rnum))/((double) (RAND_MAX))-4.0);
  }
}

int main(int argc, char* argv[]) {

  /* declarations */

  int j,N,it,nsteps;
  double *x, *y, *z, *Vx, *Vy, *Vz, *m, *Fx, *Fy, *Fz;
  double ttot,dt,comp_time;
  FILE* FID;
  clock_t start_time,finish_time; 
  
  /* set parameters */

  N=10000;
  ttot=10.0;
  nsteps=10000;
  dt=ttot/nsteps;

  /* Allocate arrays */

  x=malloc(N*sizeof(double));
  y=malloc(N*sizeof(double));
  z=malloc(N*sizeof(double));
  Vx=malloc(N*sizeof(double));
  Vy=malloc(N*sizeof(double));
  Vz=malloc(N*sizeof(double));
  Fx=malloc(N*sizeof(double));
  Fy=malloc(N*sizeof(double));
  Fz=malloc(N*sizeof(double));
  m=malloc(N*sizeof(double));

  /* Initialize - we assume the initial velocities are at t=-dt/2 */

  Initialize(N,x,y,z,Vx,Vy,Vz,m);

  start_time=clock();
  
  for (it=0; it<nsteps; it++){
    /* first the velocity step */
    Force(N,x,y,z,m,Fx,Fy,Fz);
#pragma omp parallel for shared(Fx,Fy,Fz,Vx,Vy,Vz)
    for (j=0; j<N; j++){
      Vx[j] += dt*Fx[j];
      Vy[j] += dt*Fy[j];
      Vz[j] += dt*Fz[j];
    }
  /* now position */
#pragma omp parallel for shared(x,y,z,Vx,Vy,Vz)
    for (j=0; j<N; j++){
      x[j] += dt*Vx[j];
      y[j] += dt*Vy[j];
      z[j] += dt*Vz[j];
    }
  }

  finish_time=clock();
  comp_time=((double) (finish_time-start_time))/CLOCKS_PER_SEC;
  printf("%.16e\n",comp_time);
  
  /* Output the solution */

  FID=fopen("Particle_positions10k","w");
  for (j=0; j<N; j++){
    fprintf(FID,"%.6e %.6e %.6e\n",x[j],y[j],z[j]);
  }
  fclose(FID); 
}


