/* Inclusions */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>


// Compute C = A * B
// Sgemm stands for single precision general matrix-matrix multiply
__global__ void sgemm(double *A, double *B, double *C, int numARows,
                      int numAColumns, int numBRows, int numBColumns) {

  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;
  if (row < numARows && col < numBColumns) {
    double sum = 0;
    for (int ii = 0; ii < numAColumns; ii++) {
      sum += A[row * numAColumns + ii] * B[ii * numBColumns + col];
    }
    C[row * numBColumns + col] = sum;
  }
}

int main(int argc, char **argv) {
  double *hostA; // The A matrix
  double *hostB; // The B matrix
  double *hostC; // The output C matrix
  double *hostCD; // The output C matrix as computed on the device 
  double *deviceA;
  double *deviceB;
  double *deviceC;
  int numARows=33;    // number of Rows in the matrix A
  int numAColumns=65; // number of columns in the matrix A
  int numBRows=numAColumns;    // number of Rows in the matrix B
  int numBColumns=81; // number of columns in the matrix B
  int numCRows=numARows;
  int numCColumns=numBColumns;
  double pi=3.14159265;
  double x,t,diff,ldiff,sum;
  int i,j,k;

// Allocate memory on host

  hostA = (double *)malloc(numARows*numAColumns*sizeof(double));
  hostB = (double *)malloc(numBRows*numBColumns*sizeof(double));
  hostC = (double *)malloc(numCRows*numCColumns*sizeof(double));
  hostCD = (double *)malloc(numCRows*numCColumns*sizeof(double));

// Set matrix entries

  for (i=0; i<numARows; i++){
    t=pi*((double) i)/((double) numARows-1);
    for (j=0; j<numAColumns; j++){ 
      x=((double) j)/((double) numAColumns-1);
      hostA[i*numAColumns+j]=cos(x*t);
    }
  }
  for (j=0; j<numBRows; j++){
    x=((double) j)/((double) numBRows-1);
    for (k=0; k<numBColumns; k++){ 
      hostB[j*numBColumns+k]=pow(x,k);
    }
  }
  
// Compute the product for checking

  for (i=0; i<numARows; i++){
    for (k=0; k<numBColumns; k++){ 
      sum=0.0;
      for (j=0; j<numAColumns; j++){
        sum += hostA[i*numAColumns+j]*hostB[j*numBColumns+k];
      }
      hostC[i*numCColumns+k]=sum;
    }
  }
  
  // Allocate GPU memory here
  cudaMalloc((void **)&deviceA,numARows*numAColumns*sizeof(double));
  cudaMalloc((void **)&deviceB,numBRows*numBColumns*sizeof(double));
  cudaMalloc((void **)&deviceC,numCRows*numCColumns*sizeof(double));

  // Copy memory to the GPU here
  cudaMemcpy(deviceA,hostA,numARows*numAColumns*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(deviceB,hostB,numBRows*numBColumns*sizeof(double),cudaMemcpyHostToDevice);

  // Initialize the grid and block dimensions here
  dim3 blockDim(16, 16);
  dim3 gridDim(ceil(((double)numCColumns) / blockDim.x),
               ceil(((double)numCRows) / blockDim.y));

  // Launch the GPU Kernel here
  sgemm<<<gridDim, blockDim>>>(deviceA, deviceB, deviceC, numARows,
                               numAColumns, numBRows, numBColumns);
  cudaDeviceSynchronize();

  // Copy the GPU memory back to the CPU here

  cudaMemcpy(hostCD,deviceC,numCRows*numCColumns*sizeof(double),cudaMemcpyDeviceToHost);

  // Check to see that the answers are the same

  diff=0.0;
  for (i=0; i<numCRows; i++){
    for (j=0; j<numCColumns; j++){ 
      ldiff=fabs(hostC[i*numCColumns+j]-hostCD[i*numCColumns+j]);
      if (ldiff > diff) diff=ldiff;
    }
  }

  printf("The maximum difference is %.8e\n",diff);

  // Free the GPU memory here
  cudaFree(deviceA);
  cudaFree(deviceB);
  cudaFree(deviceC);

  free(hostA);
  free(hostB);
  free(hostC);
  free(hostCD); 

  return 0;
}
