# README #

This repository contains code illustrating various things for Math 4370/6370, Spring 2019, 
Southern Methodist University. No warranties of any kind are given concerning the correctness
of the codes or their usefulness for any purpose.

Thomas Hagstrom
Dept. of Mathematics
Southern Methodist University
thagstrom@smu.edu 

