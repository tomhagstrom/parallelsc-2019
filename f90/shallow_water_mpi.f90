PROGRAM shallow_water

! Use a leap-frog method to solve the linearized shallow-water
! equations - simple ``slab'' decomposition and no output of full solution  

USE mpi
  
IMPLICIT NONE 

INTEGER, PARAMETER :: m=800  ! there will be mXm cells in the domain 
DOUBLE PRECISION :: ttot=10.d0  ! solve from time=0 to time=ttot
DOUBLE PRECISION :: c=1.d0 ! wave speed=sqrt(g*h_0)  
DOUBLE PRECISION :: cfl=.5d0 ! c*dt/dx - cannot be too large
DOUBLE PRECISION :: kx=1.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=2.d0  ! Wave number in y direction 
LOGICAL :: exsol=.TRUE. ! We have an exact solution to check accuracy 
INTEGER :: jout=40 ! Output error or solution size every jout steps 
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: h,u,v,hold,uold,vold 
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: x,y
DOUBLE PRECISION :: dx,dt,t,err,size,errloc,sizeloc,lam,lamcs,hloc,uloc,vloc,swap
DOUBLE PRECISION :: comptime,communtime,iotime,totaltime,tc1,tc2,ti1,ti2,tt1,tt2 
INTEGER :: j,k,nsteps,it,jdat,ierr,myid,numprocs,myup,mydown,mycol,mcol,mm  
INTEGER :: status_array(MPI_STATUS_SIZE,8),request(8) ! 8 different kinds of messages  
CHARACTER(3) :: write_file

! We will call a subroutine to compute the necessary initial data 
! or the exact solution if available
!
INTERFACE 
  SUBROUTINE ShallowWaterSol(hloc,uloc,vloc,xloc,yloc,t,kx,ky,c)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky,c,t
    DOUBLE PRECISION, INTENT(OUT) :: hloc,uloc,vloc 
  END SUBROUTINE ShallowWaterSol
END INTERFACE 

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD,myid,ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)

! For a slab decomposition we simply divide up the columns

mcol=m/numprocs

IF (mcol < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF 

! Allocate - note we must take account of the possibility that one slab
! is fatter than the rest - I will make it the last one

IF (myid < numprocs-1) THEN
  mm=mcol
ELSE
  mm=m-mcol*(numprocs-1)
END IF

ALLOCATE(h(0:m+1,0:mm+1),u(0:m+1,mm),v(m,0:mm+1),hold(m,mm),uold(m,mm),vold(m,mm),x(m),y(mm))

! Notice I used the fact that we only need ghost values for u and v in certain directions

! Find my neighbors

IF (myid==0) THEN
  mydown=numprocs-1  ! Periodicity
ELSE
  mydown=myid-1
END IF

IF (myid==(numprocs-1)) THEN
  myup=0  ! Periodicity
ELSE
  myup=myid+1
END IF 
  
! Set up the mesh and time step 

dx=1.d0/DBLE(m) 
dt=cfl*dx/c 
nsteps=CEILING(ttot/dt)
dt=ttot/DBLE(nsteps)
lam=dt/dx 
lamcs=lam*c*c

DO j=1,m
  x(j)=DBLE(j)*dx 
END DO

DO k=1,mm 
  y(k)=DBLE(k+myid*mcol)*dx  ! Use the fact that the only fat slab is the last one 
END DO

! Save basic input description - only use myid=0 for io 

IF (myid==0) THEN
  OPEN(UNIT=40,FILE="case1_mpi.txt",STATUS="UNKNOWN")
  WRITE(40,*)"m=",m
  WRITE(40,*)"nsteps=",nsteps
  WRITE(40,*)"ttot=",ttot
  WRITE(40,*)"c=",c
  WRITE(40,*)"kx=",kx
  WRITE(40,*)"ky=",ky
  CLOSE(UNIT=40)
END IF 

! Open output file

IF (myid==0) THEN
  OPEN(UNIT=29,FILE="sw_error_data_mpi.case1",STATUS="UNKNOWN")
END IF

! Initial data - note we need it at 2 time levels to get going
! but we assume that is taken care of in the ShallowWaterSol routine 

DO k=1,mm 
  DO j=1,m
    CALL ShallowWaterSol(h(j,k),u(j,k),v(j,k),x(j),y(k),0.d0,kx,ky,c)
  END DO
END DO

DO k=1,mm 
  DO j=1,m
    CALL ShallowWaterSol(hold(j,k),uold(j,k),vold(j,k),x(j),y(k),-dt,kx,ky,c)
  END DO
END DO 

! Now march - we start timing here

comptime=0.d0
iotime=0.d0
totaltime=0.d0 

tt1=MPI_WTIME()

DO it=1,nsteps
!
! Get data from my neighbors - first send up and receive from down
!
! I use nonblocking MPI calls - that means I can do other things
! until the data is sent/received - note that we make sure the tags mathc the message 
!
 CALL MPI_IRECV(h(1:m,0),m,MPI_DOUBLE_PRECISION,mydown,0,MPI_COMM_WORLD,request(1),ierr)
 CALL MPI_IRECV(h(1:m,mm+1),m,MPI_DOUBLE_PRECISION,myup,1,MPI_COMM_WORLD,request(2),ierr)
 CALL MPI_IRECV(v(1:m,0),m,MPI_DOUBLE_PRECISION,mydown,2,MPI_COMM_WORLD,request(3),ierr)
 CALL MPI_IRECV(v(1:m,mm+1),m,MPI_DOUBLE_PRECISION,myup,3,MPI_COMM_WORLD,request(4),ierr)
 CALL MPI_ISEND(h(1:m,1),m,MPI_DOUBLE_PRECISION,mydown,1,MPI_COMM_WORLD,request(5),ierr)
 CALL MPI_ISEND(h(1:m,mm),m,MPI_DOUBLE_PRECISION,myup,0,MPI_COMM_WORLD,request(6),ierr)
 CALL MPI_ISEND(v(1:m,1),m,MPI_DOUBLE_PRECISION,mydown,3,MPI_COMM_WORLD,request(7),ierr)
 CALL MPI_ISEND(v(1:m,mm),m,MPI_DOUBLE_PRECISION,myup,2,MPI_COMM_WORLD,request(8),ierr)
!
! We can do work while the traffic flows
!
 tc1=MPI_WTIME()
 
 h(0,1:mm)=h(m,1:mm)
 h(m+1,1:mm)=h(1,1:mm)
 u(0,1:mm)=u(m,1:mm)
 u(m+1,1:mm)=u(1,1:mm)
 

 DO k=2,mm-1
   DO j=1,m
     hold(j,k)=hold(j,k)-lam*(u(j+1,k)-u(j-1,k)+v(j,k+1)-v(j,k-1)) 
     vold(j,k)=vold(j,k)-lamcs*(h(j,k+1)-h(j,k-1)) 
   END DO
 END DO
!
 DO k=1,mm
   DO j=1,m
     uold(j,k)=uold(j,k)-lamcs*(h(j+1,k)-h(j-1,k)) 
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1
!
! Now we need to wait
!
  CALL MPI_WAITALL(8,request,status_array,ierr)
!
  tc1=MPI_WTIME() 
  
  h(0,mm+1)=h(m,mm+1)
  h(0,0)=h(m,0)
  h(m+1,mm+1)=h(1,mm+1)
  h(m+1,0)=h(1,0)
  k=1
  DO j=1,m
    hold(j,k)=hold(j,k)-lam*(u(j+1,k)-u(j-1,k)+v(j,k+1)-v(j,k-1)) 
    vold(j,k)=vold(j,k)-lamcs*(h(j,k+1)-h(j,k-1)) 
  END DO
  k=mm
  DO j=1,m
    hold(j,k)=hold(j,k)-lam*(u(j+1,k)-u(j-1,k)+v(j,k+1)-v(j,k-1)) 
    vold(j,k)=vold(j,k)-lamcs*(h(j,k+1)-h(j,k-1)) 
  END DO
  
!
! Swap 
!
 DO k=1,mm
   DO j=1,m
     swap=hold(j,k)
     hold(j,k)=h(j,k)
     h(j,k)=swap
     swap=uold(j,k)
     uold(j,k)=u(j,k)
     u(j,k)=swap
     swap=vold(j,k)
     vold(j,k)=v(j,k)
     v(j,k)=swap
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1
!
! Check for output
!
 IF (MOD(it,jout)==0) THEN

   ti1=MPI_WTIME() 
   
   t=DBLE(it)*dt
   IF (exsol) THEN
     errloc=0.d0
     sizeloc=0.d0
     DO k=1,mm
       DO j=1,m
         CALL ShallowWaterSol(hloc,uloc,vloc,x(j),y(k),t,kx,ky,c)
         errloc=errloc+dx*dx*((c*(h(j,k)-hloc))**2+(u(j,k)-uloc)**2+(v(j,k)-vloc)**2)
         sizeloc=sizeloc+dx*dx*((c*hloc)**2+uloc**2+vloc**2)
       END DO
     END DO 
     CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ierr)
     CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ierr)
     IF (myid==0) THEN
       WRITE(29,*)t,SQRT(err/size)
     END IF
   ELSE 
     sizeloc=0.d0 
     DO k=1,mm
       DO j=1,m
         sizeloc=sizeloc+dx*dx*((c*h(j,k))**2+u(j,k)**2+v(j,k)**2)
       END DO
     END DO 
     CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ierr)
     CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ierr)
     IF (myid==0) THEN
       WRITE(29,*)t,SQRT(size)
     END IF
   END IF

   ti2=MPI_WTIME()
   iotime=iotime+ti2-ti1
   
 END IF
!
END DO 

IF (myid==0) THEN
  CLOSE(UNIT=29) 
END IF

! Done 

tt2=MPI_WTIME()
totaltime=tt2-tt1
communtime=totaltime-comptime-iotime

IF (myid==0) THEN
  PRINT *,"Computation time=",comptime
  PRINT *,"IO time =",iotime
  PRINT *,"Communication time=",communtime
END IF 

CALL MPI_FINALIZE(ierr)

CONTAINS 

  SUBROUTINE PeriodicExtension 
!
! An internal subroutine knows the local variables
!
    h(0,1:m)=h(m,1:m)
    u(0,1:m)=u(m,1:m)
    v(0,1:m)=v(m,1:m)
    h(m+1,1:m)=h(1,1:m)
    u(m+1,1:m)=u(1,1:m)
    v(m+1,1:m)=v(1,1:m)
    h(0:m+1,0)=h(0:m+1,m)
    u(0:m+1,0)=u(0:m+1,m)
    v(0:m+1,0)=v(0:m+1,m)
    h(0:m+1,m+1)=h(0:m+1,1)
    u(0:m+1,m+1)=u(0:m+1,1)
    v(0:m+1,m+1)=v(0:m+1,1)
!
  END SUBROUTINE PeriodicExtension
!

END PROGRAM shallow_water 
