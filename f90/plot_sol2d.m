

x = load('x.txt');
y = load('y.txt');
mm = length(x);

sol_files = dir('h*.txt'); 

for i = 1:length(sol_files)
    h = load(sol_files(i).name);
    hp = reshape(h,mm,mm)'; 
    surf(x,y,hp)
    axis([0 1 0 1 -1 1])
    FF(i) = getframe; 
end

movie(FF) 
