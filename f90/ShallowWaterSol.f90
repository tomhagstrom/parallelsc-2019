SUBROUTINE ShallowWaterSol(h,u,v,x,y,t,kx,ky,c)
!
DOUBLE PRECISION, INTENT(IN) :: x,y,t,kx,ky,c
DOUBLE PRECISION, INTENT(OUT) :: h,u,v 
!
DOUBLE PRECISION :: pi=3.1415926535897932385d0
DOUBLE PRECISION :: kt,cx,cy,sx,sy,ct,st
!
kt=c*SQRT(kx**2+ky**2)
cx=COS(2.d0*pi*kx*x)
sx=SIN(2.d0*pi*kx*x)
cy=COS(2.d0*pi*ky*y)
sy=SIN(2.d0*pi*ky*y)
ct=COS(2.d0*pi*kt*t)
st=SIN(2.d0*pi*kt*t)
h=cx*cy*ct
u=(c*c*kx/kt)*sx*cy*st
v=(c*c*ky/kt)*cx*sy*st
!
END SUBROUTINE ShallowWaterSol 
