SUBROUTINE Source(floc,xloc,yloc,zloc,kx,ky,kz)
  DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz
  DOUBLE PRECISION, INTENT(OUT) :: floc 
!
  DOUBLE PRECISION :: pi=3.1415926535897932385d0
  DOUBLE PRECISION :: cx,sx,cy,sy,cz,sz,bg,bx,by,bz
!
  cx=COS(pi*kx*xloc)  
  sx=SIN(pi*kx*xloc)  
  cy=COS(pi*ky*yloc)  
  sy=SIN(pi*ky*yloc)  
  cz=COS(pi*kz*zloc)  
  sz=SIN(pi*kz*zloc)
  bg=10.d0*EXP(-5.d0*(xloc-.75d0)*(xloc-.75d0)-(yloc-.75d0)*(yloc-.75d0)-7.d0*(zloc-.5d0)*(zloc-.5d0))
  bx=-10.d0*(xloc-.75d0)*bg
  by=-2.d0*(yloc-.75d0)*bg
  bz=-14.d0*(zloc-.5d0)*bg
  floc=bg*pi*pi*(kx*kx+ky*ky+kz*kz)*sx*sy*sz-pi*(bx*kx*cx*sy*sz+by*ky*sx*cy*sz+bz*kz*sx*sy*cz)

END SUBROUTINE Source 

SUBROUTINE PoissonSol(uloc,xloc,yloc,zloc,kx,ky,kz)
  DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz
  DOUBLE PRECISION, INTENT(OUT) :: uloc 
!
  DOUBLE PRECISION :: pi=3.1415926535897932385d0
!
  uloc=SIN(kx*pi*xloc)*SIN(ky*pi*yloc)*SIN(kz*pi*zloc)
!
END SUBROUTINE PoissonSol

SUBROUTINE diffun(sigx,sigy,sigz,xloc,yloc,zloc,dx,dy,dz)
  DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,dx,dy,dz
  DOUBLE PRECISION, DIMENSION(2), INTENT(OUT) :: sigx,sigy,sigz
!
  DOUBLE PRECISION :: dxh,dyh,dzh
!
  dxh=.5d0*dx
  dyh=.5d0*dy
  dzh=.5d0*dz
!
  sigx(1)=10.d0*EXP(-5.d0*(xloc-dxh-.75d0)*(xloc-dxh-.75d0)-(yloc-.75d0)*(yloc-.75d0)-7.d0*(zloc-.5d0)*(zloc-.5d0))
  sigx(2)=10.d0*EXP(-5.d0*(xloc+dxh-.75d0)*(xloc+dxh-.75d0)-(yloc-.75d0)*(yloc-.75d0)-7.d0*(zloc-.5d0)*(zloc-.5d0))
  sigy(1)=10.d0*EXP(-5.d0*(xloc-.75d0)*(xloc-.75d0)-(yloc-dyh-.75d0)*(yloc-dyh-.75d0)-7.d0*(zloc-.5d0)*(zloc-.5d0))
  sigy(2)=10.d0*EXP(-5.d0*(xloc-.75d0)*(xloc-.75d0)-(yloc+dyh-.75d0)*(yloc+dyh-.75d0)-7.d0*(zloc-.5d0)*(zloc-.5d0))
  sigz(1)=10.d0*EXP(-5.d0*(xloc-.75d0)*(xloc-.75d0)-(yloc-.75d0)*(yloc-.75d0)-7.d0*(zloc-dzh-.5d0)*(zloc-dzh-.5d0))
  sigz(2)=10.d0*EXP(-5.d0*(xloc-.75d0)*(xloc-.75d0)-(yloc-.75d0)*(yloc-.75d0)-7.d0*(zloc+dzh-.5d0)*(zloc+dzh-.5d0))
!
END SUBROUTINE diffun   


