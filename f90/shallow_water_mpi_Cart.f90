PROGRAM shallow_water

! Use a leap-frog method to solve the linearized shallow-water
! equations - Cartesian decomposition and no output of full solution  

USE mpi
  
IMPLICIT NONE 

INTEGER, PARAMETER :: m=800  ! there will be mXm cells in the domain 
DOUBLE PRECISION :: ttot=10.d0  ! solve from time=0 to time=ttot
DOUBLE PRECISION :: c=1.d0 ! wave speed=sqrt(g*h_0)  
DOUBLE PRECISION :: cfl=.5d0 ! c*dt/dx - cannot be too large
DOUBLE PRECISION :: kx=1.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=2.d0  ! Wave number in y direction 
LOGICAL :: exsol=.TRUE. ! We have an exact solution to check accuracy 
INTEGER :: jout=40 ! Output error or solution size every jout steps 
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: h,u,v,hold,uold,vold 
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: x,y,hsl,hrl,hsr,hrr,hsu,hru,hsd,hrd,usl,url,usr,urr,vsu,vru,vsd,vrd
DOUBLE PRECISION :: dx,dy,dt,t,err,size,errloc,sizeloc,lam,lamcs,hloc,uloc,vloc,swap
DOUBLE PRECISION :: comptime,communtime,iotime,tc1,tc2,ti1,ti2,tt1,tt2 
INTEGER :: j,k,nsteps,it,jdat,ierr,myid,numprocs,myup,mydown,myleft,myright,myxl,myxr,myyd,myyu,leftover,mmx,mmy
INTEGER :: status_array(MPI_STATUS_SIZE,16),request(16) ! 16 different kinds of messages  
CHARACTER(3) :: write_file
!
INTEGER, DIMENSION(2) :: dims,mycoords
LOGICAL, DIMENSION(2) :: isperiodic=(/ .TRUE. , .TRUE. /) 
LOGICAL :: reorder=.TRUE.
INTEGER :: newcomm 

! We will call a subroutine to compute the necessary initial data 
! or the exact solution if available
!
INTERFACE 
  SUBROUTINE ShallowWaterSol(hloc,uloc,vloc,xloc,yloc,t,kx,ky,c)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky,c,t
    DOUBLE PRECISION, INTENT(OUT) :: hloc,uloc,vloc 
  END SUBROUTINE ShallowWaterSol
END INTERFACE 

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)

! Roughly we want to break the domain into squarish pieces - we use an MPI call to do it

CALL MPI_DIMS_CREATE(numprocs,2,dims,ierr)

CALL MPI_CART_CREATE(MPI_COMM_WORLD,2,dims,isperiodic,reorder,newcomm,ierr)

! Neighbors and coordinates

CALL MPI_COMM_RANK(newcomm,myid,ierr)
CALL MPI_CART_SHIFT(newcomm,0,1,myleft,myright,ierr)
CALL MPI_CART_SHIFT(newcomm,1,1,mydown,myup,ierr)
CALL MPI_CART_COORDS(newcomm,myid,2,mycoords,ierr)

! Now we divvy out the data - I will improve on what I did in the first code
! which could cause the last processor to be overloaded - here I will determine
! left and right indices and up and down indices for each process

leftover=MOD(m,dims(1)) 
mmx=m/dims(1)

IF (mmx < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(1) <= leftover) THEN
   myxl=(mmx+1)*mycoords(1)+1
ELSE
   myxl=(mmx+1)*leftover+mmx*(mycoords(1)-leftover)+1
END IF

IF (mycoords(1) < leftover) THEN
   myxr=myxl+mmx
ELSE
   myxr=myxl+mmx-1
END IF 

mmx=myxr-myxl+1 ! for later use

leftover=MOD(m,dims(2)) 
mmy=m/dims(2)

IF (mmy < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(2) <= leftover) THEN
   myyd=(mmy+1)*mycoords(2)+1
ELSE
   myyd=(mmy+1)*leftover+mmy*(mycoords(2)-leftover)+1
END IF

IF (mycoords(2) < leftover) THEN
   myyu=myyd+mmy
ELSE
   myyu=myyd+mmy-1
END IF 

mmy=myyu-myyd+1 ! for later use

! Now we can allocate 

ALLOCATE(h(myxl-1:myxr+1,myyd-1:myyu+1),u(myxl-1:myxr+1,myyd:myyu),v(myxl:myxr,myyd-1:myyu+1), &
     hold(myxl:myxr,myyd:myyu),uold(myxl:myxr,myyd:myyu),vold(myxl:myxr,myyd:myyu),x(myxl:myxr),y(myyd:myyu), &
     hsl(mmy),hrl(mmy),hsr(mmy),hrr(mmy),usl(mmy),url(mmy),usr(mmy),urr(mmy), &
     hsu(mmx),hru(mmx),hsd(mmx),hrd(mmx),vsu(mmx),vru(mmx),vsd(mmx),vrd(mmx)) 

! Notice I used the fact that we only need ghost values for u and v in certain directions

! Set up the mesh and time step 

dx=1.d0/DBLE(m)
dy=dx
dt=cfl*dx/c 
nsteps=CEILING(ttot/dt)
dt=ttot/DBLE(nsteps)
lam=dt/dx 
lamcs=lam*c*c

DO j=myxl,myxr
  x(j)=DBLE(j)*dx 
END DO

DO k=myyd,myyu
  y(k)=DBLE(k)*dy  
END DO

! Save basic input description - only use myid=0 for io 

IF (myid==0) THEN
  OPEN(UNIT=40,FILE="case1_mpi_Cart.txt",STATUS="UNKNOWN")
  WRITE(40,*)"m=",m
  WRITE(40,*)"nsteps=",nsteps
  WRITE(40,*)"ttot=",ttot
  WRITE(40,*)"c=",c
  WRITE(40,*)"kx=",kx
  WRITE(40,*)"ky=",ky
  CLOSE(UNIT=40)
END IF 

! Open output file

IF (myid==0) THEN
  OPEN(UNIT=29,FILE="sw_error_data_mpi_Cart.case1",STATUS="UNKNOWN")
END IF

! Initial data - note we need it at 2 time levels to get going
! but we assume that is taken care of in the ShallowWaterSol routine 

DO k=myyd,myyu
  DO j=myxl,myxr
    CALL ShallowWaterSol(h(j,k),u(j,k),v(j,k),x(j),y(k),0.d0,kx,ky,c)
  END DO
END DO

DO k=myyd,myyu
  DO j=myxl,myxr
    CALL ShallowWaterSol(hold(j,k),uold(j,k),vold(j,k),x(j),y(k),-dt,kx,ky,c)
  END DO
END DO 

! Now march - we start timing here

comptime=0.d0
iotime=0.d0
communtime=0.d0 

DO it=1,nsteps 
!
! Get data from my neighbors 
!
! I use nonblocking MPI calls - note that we make sure the tags match the message 
!

 tt1=MPI_WTIME()

 hsu=h(myxl:myxr,myyu)
 hsd=h(myxl:myxr,myyd)
 vsu=v(myxl:myxr,myyu)
 vsd=v(myxl:myxr,myyd)
 hsr=h(myxr,myyd:myyu)
 hsl=h(myxl,myyd:myyu)
 usr=u(myxr,myyd:myyu)
 usl=u(myxl,myyd:myyu)
 CALL MPI_IRECV(hrd,mmx,MPI_DOUBLE_PRECISION,mydown,0,MPI_COMM_WORLD,request(1),ierr)
 CALL MPI_ISEND(hsu,mmx,MPI_DOUBLE_PRECISION,myup,0,MPI_COMM_WORLD,request(2),ierr)
 CALL MPI_IRECV(hru,mmx,MPI_DOUBLE_PRECISION,myup,1,MPI_COMM_WORLD,request(3),ierr)
 CALL MPI_ISEND(hsd,mmx,MPI_DOUBLE_PRECISION,mydown,1,MPI_COMM_WORLD,request(4),ierr)
 CALL MPI_IRECV(vrd,mmx,MPI_DOUBLE_PRECISION,mydown,2,MPI_COMM_WORLD,request(5),ierr)
 CALL MPI_ISEND(vsu,mmx,MPI_DOUBLE_PRECISION,myup,2,MPI_COMM_WORLD,request(6),ierr)
 CALL MPI_IRECV(vru,mmx,MPI_DOUBLE_PRECISION,myup,3,MPI_COMM_WORLD,request(7),ierr)
 CALL MPI_ISEND(vsd,mmx,MPI_DOUBLE_PRECISION,mydown,3,MPI_COMM_WORLD,request(8),ierr)
 CALL MPI_IRECV(hrl,mmy,MPI_DOUBLE_PRECISION,myleft,4,MPI_COMM_WORLD,request(9),ierr)
 CALL MPI_ISEND(hsr,mmy,MPI_DOUBLE_PRECISION,myright,4,MPI_COMM_WORLD,request(10),ierr)
 CALL MPI_IRECV(hrr,mmy,MPI_DOUBLE_PRECISION,myright,5,MPI_COMM_WORLD,request(11),ierr)
 CALL MPI_ISEND(hsl,mmy,MPI_DOUBLE_PRECISION,myleft,5,MPI_COMM_WORLD,request(12),ierr)
 CALL MPI_IRECV(url,mmy,MPI_DOUBLE_PRECISION,myleft,6,MPI_COMM_WORLD,request(13),ierr)
 CALL MPI_ISEND(usr,mmy,MPI_DOUBLE_PRECISION,myright,6,MPI_COMM_WORLD,request(14),ierr)
 CALL MPI_IRECV(urr,mmy,MPI_DOUBLE_PRECISION,myright,7,MPI_COMM_WORLD,request(15),ierr)
 CALL MPI_ISEND(usl,mmy,MPI_DOUBLE_PRECISION,myleft,7,MPI_COMM_WORLD,request(16),ierr)
!
! Now we wait
!
 CALL MPI_WAITALL(16,request,status_array,ierr)
!
 h(myxr+1,myyd:myyu)=hrr
 h(myxl-1,myyd:myyu)=hrl
 u(myxr+1,myyd:myyu)=urr
 u(myxl-1,myyd:myyu)=url
 h(myxl:myxr,myyu+1)=hru
 h(myxl:myxr,myyd-1)=hrd
 v(myxl:myxr,myyu+1)=vru
 v(myxl:myxr,myyd-1)=vrd
!
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1
!
 tc1=MPI_WTIME()
 
 DO k=myyd,myyu
   DO j=myxl,myxr
     hold(j,k)=hold(j,k)-lam*(u(j+1,k)-u(j-1,k)+v(j,k+1)-v(j,k-1)) 
     uold(j,k)=uold(j,k)-lamcs*(h(j+1,k)-h(j-1,k)) 
     vold(j,k)=vold(j,k)-lamcs*(h(j,k+1)-h(j,k-1)) 
   END DO
 END DO

!
! Swap 
!
 DO k=myyd,myyu
   DO j=myxl,myxr
     swap=hold(j,k)
     hold(j,k)=h(j,k)
     h(j,k)=swap
     swap=uold(j,k)
     uold(j,k)=u(j,k)
     u(j,k)=swap
     swap=vold(j,k)
     vold(j,k)=v(j,k)
     v(j,k)=swap
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1
!
! Check for output
!
 IF (MOD(it,jout)==0) THEN

   ti1=MPI_WTIME()
   
   t=DBLE(it)*dt
   IF (exsol) THEN
     errloc=0.d0
     sizeloc=0.d0
     DO k=myyd,myyu
       DO j=myxl,myxr
         CALL ShallowWaterSol(hloc,uloc,vloc,x(j),y(k),t,kx,ky,c)
         errloc=errloc+dx*dx*((c*(h(j,k)-hloc))**2+(u(j,k)-uloc)**2+(v(j,k)-vloc)**2)
         sizeloc=sizeloc+dx*dx*((c*hloc)**2+uloc**2+vloc**2)
       END DO
     END DO 
     CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
     CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
     IF (myid==0) THEN
       WRITE(29,*)t,SQRT(err/size)
     END IF
   ELSE 
     sizeloc=0.d0 
     DO k=myyd,myyu
       DO j=myxl,myxr
         sizeloc=sizeloc+dx*dx*((c*h(j,k))**2+u(j,k)**2+v(j,k)**2)
       END DO
     END DO 
     CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
     CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
     IF (myid==0) THEN
       WRITE(29,*)t,SQRT(size)
     END IF
   END IF

   ti2=MPI_WTIME()
   iotime=iotime+ti2-ti1
   
 END IF
!
END DO 

IF (myid==0) THEN
  CLOSE(UNIT=29) 
END IF

! Done 

IF (myid==0) THEN
  PRINT *,"Computation time=",comptime
  PRINT *,"IO time =",iotime
  PRINT *,"Communication time=",communtime
END IF 

CALL MPI_FINALIZE(ierr)

END PROGRAM shallow_water 
