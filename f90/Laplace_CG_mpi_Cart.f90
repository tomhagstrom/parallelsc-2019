PROGRAM Laplace_CG

! Use second order differences and the conjugate gradient method 
! to solve the Poisson problem - Cartesian decomposition and no output of full solution  

USE mpi
  
IMPLICIT NONE 

INTEGER, PARAMETER :: m=800  ! there will be mXm cells in the domain 
DOUBLE PRECISION :: ctol=1.d-6  ! solve until the residual norm < ctol - note that inner products will effectively 
                                ! scale by dx*dy 
DOUBLE PRECISION :: kx=5.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=7.d0  ! Wave number in y direction 
LOGICAL :: exsol=.TRUE. ! We have an exact solution to check accuracy 
INTEGER :: itmax=10000 ! Quit if we need too many iterations 
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: u,z,p,r
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: x,y,psl,prl,psr,prr,psu,pru,psd,prd
DOUBLE PRECISION :: dx,dy,err,size,errloc,sizeloc,snew,snewloc,ptz,ptzloc,mu,nu,uloc  
DOUBLE PRECISION :: comptime,communtime,iotime,tc1,tc2,ti1,ti2,tt1,tt2 
INTEGER :: j,k,it,ierr,myid,numprocs,myup,mydown,myleft,myright,myxl,myxr,myyd,myyu,leftover,mmx,mmy
INTEGER :: status_array(MPI_STATUS_SIZE,8),request(8) ! 8 different kinds of messages  
CHARACTER(3) :: write_file
!
INTEGER, DIMENSION(2) :: dims,mycoords
LOGICAL, DIMENSION(2) :: isperiodic=(/ .FALSE. , .FALSE. /) 
LOGICAL :: reorder=.TRUE.
INTEGER :: newcomm 

! We will call a subroutine to compute the forcing function 

INTERFACE 
  SUBROUTINE Source(floc,xloc,yloc,kx,ky)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky
    DOUBLE PRECISION, INTENT(OUT) :: floc 
  END SUBROUTINE Source 
END INTERFACE 

! We will call another subroutine to evaluate the exact solution for testing
INTERFACE 
  SUBROUTINE PoissonSol(uloc,xloc,yloc,kx,ky)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky
    DOUBLE PRECISION, INTENT(OUT) :: uloc 
  END SUBROUTINE PoissonSol 
END INTERFACE 

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)

! Roughly we want to break the domain into squarish pieces - we use an MPI call to do it

CALL MPI_DIMS_CREATE(numprocs,2,dims,ierr)

CALL MPI_CART_CREATE(MPI_COMM_WORLD,2,dims,isperiodic,reorder,newcomm,ierr)

! Neighbors and coordinates

CALL MPI_COMM_RANK(newcomm,myid,ierr)
CALL MPI_CART_SHIFT(newcomm,0,1,myleft,myright,ierr)
CALL MPI_CART_SHIFT(newcomm,1,1,mydown,myup,ierr)
CALL MPI_CART_COORDS(newcomm,myid,2,mycoords,ierr)

! Notice that the MPI_CART commands take care of not having a neighbor
! when we are at a boundary - but in the code I will be careful 

! Now we divvy out the data - as in shallow_water_mpi_Cart

leftover=MOD(m-1,dims(1)) 
mmx=(m-1)/dims(1)

IF (mmx < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(1) <= leftover) THEN
   myxl=(mmx+1)*mycoords(1)+1
ELSE
   myxl=(mmx+1)*leftover+mmx*(mycoords(1)-leftover)+1
END IF

IF (mycoords(1) < leftover) THEN
   myxr=myxl+mmx
ELSE
   myxr=myxl+mmx-1
END IF 

mmx=myxr-myxl+1 ! for later use

leftover=MOD(m-1,dims(2)) 
mmy=(m-1)/dims(2)

IF (mmy < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(2) <= leftover) THEN
   myyd=(mmy+1)*mycoords(2)+1
ELSE
   myyd=(mmy+1)*leftover+mmy*(mycoords(2)-leftover)+1
END IF

IF (mycoords(2) < leftover) THEN
   myyu=myyd+mmy
ELSE
   myyu=myyd+mmy-1
END IF 

mmy=myyu-myyd+1 ! for later use

! Now we can allocate 

ALLOCATE(p(myxl-1:myxr+1,myyd-1:myyu+1),u(myxl:myxr,myyd:myyu),z(myxl:myxr,myyd:myyu),r(myxl:myxr,myyd:myyu), &
     psl(mmy),prl(mmy),psr(mmy),prr(mmy),psu(mmx),pru(mmx),psd(mmx),prd(mmx),x(myxl:myxr),y(myyd:myyu)) 

! Notice I used the fact that we only need ghost values for p and we only need to exchange p 

! Set up the mesh

dx=1.d0/DBLE(m)
dy=dx

DO j=myxl,myxr
  x(j)=DBLE(j)*dx 
END DO

DO k=myyd,myyu
  y(k)=DBLE(k)*dy  
END DO

! Save basic input description - only use myid=0 for io 

IF (myid==0) THEN
  OPEN(UNIT=40,FILE="case3_Poisson.txt",STATUS="UNKNOWN")
  WRITE(40,*)"m=",m
  WRITE(40,*)"kx=",kx
  WRITE(40,*)"ky=",ky
  CLOSE(UNIT=40)
END IF 

! Open output file

IF (myid==0) THEN
  OPEN(UNIT=29,FILE="Poisson_convergence_error_data.case3",STATUS="UNKNOWN")
END IF

! Initialize - the initial guess for u is zero so the initial residual is
! simply the source function 

DO k=myyd,myyu
  DO j=myxl,myxr
    CALL Source(r(j,k),x(j),y(k),kx,ky)
  END DO
END DO

p=0.d0
p(myxl:myxr,myyd:myyu)=r(myxl:myxr,myyd:myyu)
u=0.d0 

! Now iterate - we start timing here

comptime=0.d0
iotime=0.d0
communtime=0.d0 

! I will use size to keep track of r^T r 

tc1=MPI_WTIME()

sizeloc=0.d0
DO k=myyd,myyu
  DO j=myxl,myxr
    sizeloc=sizeloc+r(j,k)*r(j,k) 
  END DO
END DO

tc2=MPI_WTIME()
comptime=comptime+tc2-tc1

tt1=MPI_WTIME()

CALL MPI_ALLREDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)

tt2=MPI_WTIME()
communtime=communtime+tt2-tt1

it=1

ti1=MPI_WTIME()

IF (myid==0) THEN
  WRITE (29,*)it,SQRT(size)
END IF 

ti2=MPI_WTIME() 
iotime=iotime+ti2-ti1

DO WHILE ((it <= itmax).AND.(SQRT(size) > ctol))

!
! Get data from my neighbors 
!
! I use nonblocking MPI calls - note that we make sure the tags match the message 
!

 tt1=MPI_WTIME()

 psu=p(myxl:myxr,myyu)
 psd=p(myxl:myxr,myyd)
 psr=p(myxr,myyd:myyu)
 psl=p(myxl,myyd:myyu)
 CALL MPI_IRECV(prd,mmx,MPI_DOUBLE_PRECISION,mydown,0,MPI_COMM_WORLD,request(1),ierr)
 CALL MPI_ISEND(psu,mmx,MPI_DOUBLE_PRECISION,myup,0,MPI_COMM_WORLD,request(2),ierr)
 CALL MPI_IRECV(pru,mmx,MPI_DOUBLE_PRECISION,myup,1,MPI_COMM_WORLD,request(3),ierr)
 CALL MPI_ISEND(psd,mmx,MPI_DOUBLE_PRECISION,mydown,1,MPI_COMM_WORLD,request(4),ierr)
 CALL MPI_IRECV(prl,mmy,MPI_DOUBLE_PRECISION,myleft,2,MPI_COMM_WORLD,request(5),ierr)
 CALL MPI_ISEND(psr,mmy,MPI_DOUBLE_PRECISION,myright,2,MPI_COMM_WORLD,request(6),ierr)
 CALL MPI_IRECV(prr,mmy,MPI_DOUBLE_PRECISION,myright,3,MPI_COMM_WORLD,request(7),ierr)
 CALL MPI_ISEND(psl,mmy,MPI_DOUBLE_PRECISION,myleft,3,MPI_COMM_WORLD,request(8),ierr)
!
! Now we wait
!
 CALL MPI_WAITALL(8,request,status_array,ierr)
!
 IF (mycoords(1) < (dims(1)-1)) THEN
   p(myxr+1,myyd:myyu)=prr
 ELSE
   p(myxr+1,myyd:myyu)=0.d0
 END IF 
 IF (mycoords(1) > 0) THEN
   p(myxl-1,myyd:myyu)=prl
 ELSE
   p(myxl-1,myyd:myyu)=0.d0 
 END IF 
 IF (mycoords(2) < (dims(2)-1))  THEN
   p(myxl:myxr,myyu+1)=pru
 ELSE
   p(myxl:myxr,myyu+1)=0.d0
 END IF
 IF (mycoords(2) > 0) THEN 
   p(myxl:myxr,myyd-1)=prd
 ELSE
   p(myxl:myxr,myyd-1)=0.d0
 END IF 
!
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1
!
 tc1=MPI_WTIME()
 
 z=0.d0
 DO k=myyd,myyu
   DO j=myxl,myxr
     z(j,k)=(-p(j+1,k)-p(j-1,k)+2.d0*p(j,k))/(dx*dx)+(-p(j,k+1)-p(j,k-1)+2.d0*p(j,k))/(dy*dy)
   END DO
 END DO

 ptzloc=0.d0
 DO k=myyd,myyu
   DO j=myxl,myxr
     ptzloc=ptzloc+p(j,k)*z(j,k)
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1
 
 tt1=MPI_WTIME()

 CALL MPI_ALLREDUCE(ptzloc,ptz,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)
  
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1

 tc1=MPI_WTIME()

 nu=size/ptz

 DO k=myyd,myyu
   DO j=myxl,myxr
     u(j,k)=u(j,k)+nu*p(j,k)
   END DO
 END DO

 DO k=myyd,myyu
   DO j=myxl,myxr
     r(j,k)=r(j,k)-nu*z(j,k)
   END DO
 END DO

 snewloc=0.d0
 DO k=myyd,myyu
   DO j=myxl,myxr
     snewloc=snewloc+r(j,k)*r(j,k) 
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1

 tt1=MPI_WTIME()

 CALL MPI_ALLREDUCE(snewloc,snew,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)
 
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1

 tc1=MPI_WTIME()

 mu=snew/size
 size=snew 

 DO k=myyd,myyu
   DO j=myxl,myxr
     p(j,k)=r(j,k)+mu*p(j,k)
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1

 it=it+1

 ti1=MPI_WTIME()

 IF (myid==0) THEN
   WRITE (29,*)it,SQRT(size)
 END IF 

 ti2=MPI_WTIME() 
 iotime=iotime+ti2-ti1

END DO

ti1=MPI_WTIME()

IF (exsol) THEN
  errloc=0.d0
  sizeloc=0.d0
  DO k=myyd,myyu
    DO j=myxl,myxr
      CALL PoissonSol(uloc,x(j),y(k),kx,ky)
      errloc=errloc+dx*dy*(u(j,k)-uloc)**2
      sizeloc=sizeloc+dx*dy*uloc**2
    END DO
  END DO 
  CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  IF (myid==0) THEN
    WRITE(29,*)"Final error:",SQRT(err/size)
  END IF
ELSE 
  sizeloc=0.d0 
  DO k=myyd,myyu
    DO j=myxl,myxr
      sizeloc=sizeloc+dx*dy*u(j,k)**2
    END DO
  END DO 
  CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  IF (myid==0) THEN
    WRITE(29,*)"Solution size:",SQRT(size)
  END IF
END IF

ti2=MPI_WTIME()
iotime=iotime+ti2-ti1
   
IF (myid==0) THEN
  CLOSE(UNIT=29) 
END IF

! Done 

IF (myid==0) THEN
  PRINT *,"Computation time=",comptime
  PRINT *,"IO time =",iotime
  PRINT *,"Communication time=",communtime
END IF 

CALL MPI_FINALIZE(ierr)

END PROGRAM Laplace_CG  
