PROGRAM Laplace_Hypre

!   A program using Hypre to solve Laplace's equation in 3d 
!   built off of examples 3 and 4 in the Hypre examples directory
!   using their interface for structured grids 

USE mpi

IMPLICIT NONE

INCLUDE 'HYPREf.h'

INTEGER, PARAMETER :: m=800 ! m^3 mesh 
INTEGER :: itmax=200 ! maximum iterations 
DOUBLE PRECISION :: ctol=1.d-6  ! tolerance for iterations
DOUBLE PRECISION :: kx=1.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=1.d0  ! Wave number in y direction 
DOUBLE PRECISION :: kz=1.d0  ! Wave number in z direction 
INTEGER :: i,j,k,ict,ierr,nvalues
DOUBLE PRECISION :: err,errloc,size,sizeloc,uloc
DOUBLE PRECISION, DIMENSION(2) :: sigx,sigy,sigz
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: values
INTEGER :: myid,numprocs,mmx,mmy,mmz,leftover
LOGICAL :: exsol=.TRUE.
DOUBLE PRECISION :: dx,dy,dz,xloc,yloc,zloc,final_res_norm
INTEGER :: solver_id,n_pre,n_post,num_iterations
INTEGER, DIMENSION(3) :: dims,mycoords
LOGICAL, DIMENSION(3) :: isperiodic=(/ .FALSE. , .FALSE. , .FALSE. /) 
LOGICAL :: reorder=.TRUE.
INTEGER :: newcomm
INTEGER, DIMENSION(3,4) :: offsets
INTEGER, DIMENSION(4) :: stencil_indices
INTEGER, DIMENSION(1) :: bc_stencil_index 
INTEGER, DIMENSION(3) :: iupper,ilower,bc_iupper,bc_ilower
INTEGER*8 :: grid,stencil,A,b,x,solver,precond  ! Hypre structs 

! We will call a subroutine to compute the forcing function 

INTERFACE 
  SUBROUTINE Source(floc,xloc,yloc,zloc,kx,ky,kz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz 
    DOUBLE PRECISION, INTENT(OUT) :: floc 
  END SUBROUTINE Source 
END INTERFACE 

! We will call another subroutine to evaluate the exact solution for testing
INTERFACE 
  SUBROUTINE PoissonSol(uloc,xloc,yloc,zloc,kx,ky,kz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz 
    DOUBLE PRECISION, INTENT(OUT) :: uloc 
  END SUBROUTINE PoissonSol 
END INTERFACE 

! And finally one for the diffusion coefficients
INTERFACE
  SUBROUTINE diffun(sigx,sigy,sigz,xloc,yloc,zloc,dx,dy,dz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,dx,dy,dz
    DOUBLE PRECISION, DIMENSION(2), INTENT(OUT) :: sigx,sigy,sigz
  END SUBROUTINE diffun
END INTERFACE

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)

! Roughly we want to break the domain into squarish pieces - we use an MPI call to do it

!CALL MPI_DIMS_CREATE(numprocs,3,dims,ierr)

dims(1)=3
dims(2)=6
dims(3)=4 

CALL MPI_CART_CREATE(MPI_COMM_WORLD,3,dims,isperiodic,reorder,newcomm,ierr)

CALL MPI_COMM_RANK(newcomm,myid,ierr)
CALL MPI_CART_COORDS(newcomm,myid,3,mycoords,ierr)

leftover=MOD(m,dims(1)) 
mmx=m/dims(1)

IF (mmx < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(1) <= leftover) THEN
   ilower(1)=(mmx+1)*mycoords(1)
ELSE
   ilower(1)=(mmx+1)*leftover+mmx*(mycoords(1)-leftover)
END IF

IF (mycoords(1) < leftover) THEN
   iupper(1)=ilower(1)+mmx
ELSE
   iupper(1)=ilower(1)+mmx-1
END IF 

mmx=iupper(1)-ilower(1)+1 ! for later use

leftover=MOD(m,dims(2)) 
mmy=m/dims(2)

IF (mmy < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(2) <= leftover) THEN
   ilower(2)=(mmy+1)*mycoords(2)
ELSE
   ilower(2)=(mmy+1)*leftover+mmy*(mycoords(2)-leftover)
END IF

IF (mycoords(2) < leftover) THEN
   iupper(2)=ilower(2)+mmy
ELSE
   iupper(2)=ilower(2)+mmy-1
END IF 

mmy=iupper(2)-ilower(2)+1 ! for later use

leftover=MOD(m,dims(3)) 
mmz=m/dims(3)

IF (mmz < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(3) <= leftover) THEN
   ilower(3)=(mmz+1)*mycoords(3)
ELSE
   ilower(3)=(mmz+1)*leftover+mmz*(mycoords(3)-leftover)
END IF

IF (mycoords(3) < leftover) THEN
   iupper(3)=ilower(3)+mmz
ELSE
   iupper(3)=ilower(3)+mmz-1
END IF 

mmz=iupper(3)-ilower(3)+1 ! for later use

! Set parameters for hypre 

solver_id=0 ! PCG with symmetric multigrid 
n_pre=1  ! prerelaxations 
n_post=1 ! postrelaxations 

! Set up the mesh 

dx=1.d0/DBLE(m+1)
dy=dx
dz=dx

! Save basic input information - we do all io on myid=0 

IF (myid==0) THEN
  OPEN(UNIT=19,FILE="Laplace_Hypre.case3",STATUS="UNKNOWN")
  WRITE(19,*)"m=",m
  WRITE(19,*)"numprocs=",numprocs
  WRITE(19,*)"Hypre preconditioner=",solver_id
END IF

! Set up a grid 
! Create an empty 3D grid object

CALL HYPRE_StructGridCreate(newcomm,3,grid,ierr)

! Add a new box to the grid

CALL HYPRE_StructGridSetExtents(grid,ilower,iupper,ierr)

! This is a collective call finalizing the grid assembly.
!         The grid is now ``ready to be used''

CALL HYPRE_StructGridAssemble(grid,ierr)

! Define the discretization stencil using symmetric storage 
! Define the geometry of the stencil

offsets(1,1)=0
offsets(2,1)=0
offsets(3,1)=0
offsets(1,2)=1
offsets(2,2)=0
offsets(3,2)=0
offsets(1,3)=0
offsets(2,3)=1
offsets(3,3)=0
offsets(1,4)=0
offsets(2,4)=0
offsets(3,4)=1

! Create an empty 3D, 4-pt stencil object

CALL HYPRE_StructStencilCreate(3,4,stencil,ierr)

! Assign stencil entries

DO i=1,4
  CALL HYPRE_StructStencilSetElement(stencil,i-1,offsets(:,i),ierr)
END DO

nvalues=4*mmx*mmy*mmz
ALLOCATE(values(nvalues))

! Create an empty matrix object 

CALL HYPRE_StructMatrixCreate(newcomm,grid,stencil,A,ierr)

! Use symmetric storage

CALL HYPRE_StructMatrixSetSymmetric(A,1,ierr)

! Indicate that the matrix coefficients are ready to be set

CALL HYPRE_StructMatrixInitialize(A,ierr)

DO j=1,4
  stencil_indices(j)=j-1
END DO

! Set the standard stencil at each grid point, we will fix the boundaries later

ict=1
DO k=1,mmz
  DO j=1,mmy
    DO i=1,mmx
      xloc=dx*DBLE(ilower(1)+i)
      yloc=dy*DBLE(ilower(2)+j)
      zloc=dz*DBLE(ilower(3)+k)
      CALL diffun(sigx,sigy,sigz,xloc,yloc,zloc,dx,dy,dz)
      values(ict+1)=-sigx(2)/(dx*dx)
      values(ict+2)=-sigy(2)/(dy*dy)
      values(ict+3)=-sigz(2)/(dz*dz)
      values(ict)=(sigx(1)+sigx(2))/(dx*dx)+(sigy(1)+sigy(2))/(dy*dy)+(sigz(1)+sigz(2))/(dz*dz)
      ict=ict+4
    END DO
  END DO
END DO

CALL HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,4,stencil_indices,values,ierr)

DEALLOCATE(values)

!    Incorporate the zero boundary conditions: go along each face of
!    the domain and set the stencil entry that reaches to the boundary to
!    zero - start with the faces with normal x - since we assume symmetry we 
!    only need to deal with half the boundaries - start with the right

! check using mycoords if I am on the right 

IF (mycoords(1) == dims(1)-1) THEN
  nvalues=mmy*mmz
  ALLOCATE(values(nvalues))
  values=0.d0
  bc_ilower(1)=iupper(1)
  bc_ilower(2)=ilower(2)
  bc_ilower(3)=ilower(3)
  bc_iupper(1)=iupper(1)
  bc_iupper(2)=iupper(2)
  bc_iupper(3)=iupper(3)
  bc_stencil_index(1)=1
  CALL HYPRE_StructMatrixSetBoxValues(A,bc_ilower,bc_iupper,1,bc_stencil_index,values,ierr)
  DEALLOCATE(values)
END IF

! check using mycoords if I am on top 

IF (mycoords(2) == dims(2)-1) THEN
  nvalues=mmx*mmz
  ALLOCATE(values(nvalues))
  values=0.d0
  bc_ilower(1)=ilower(1)
  bc_ilower(2)=iupper(2)
  bc_ilower(3)=ilower(3)
  bc_iupper(1)=iupper(1)
  bc_iupper(2)=iupper(2)
  bc_iupper(3)=iupper(3)
  bc_stencil_index(1)=2
  CALL HYPRE_StructMatrixSetBoxValues(A,bc_ilower,bc_iupper,1,bc_stencil_index,values,ierr)
  DEALLOCATE(values)
END IF

! check using mycoords if I am up front 

IF (mycoords(3) == dims(3)-1) THEN
  nvalues=mmx*mmy
  ALLOCATE(values(nvalues))
  values=0.d0
  bc_ilower(1)=ilower(1)
  bc_ilower(2)=ilower(2)
  bc_ilower(3)=iupper(3)
  bc_iupper(1)=iupper(1)
  bc_iupper(2)=iupper(2)
  bc_iupper(3)=iupper(3)
  bc_stencil_index(1)=3
  CALL HYPRE_StructMatrixSetBoxValues(A,bc_ilower,bc_iupper,1,bc_stencil_index,values,ierr)
  DEALLOCATE(values)
END IF

! This is a collective call finalizing the matrix assembly.
! The matrix is now ``ready to be used''

CALL HYPRE_StructMatrixAssemble(A,ierr)

! Set up Struct Vectors for b and x 

! Create an empty vector object

CALL HYPRE_StructVectorCreate(newcomm,grid,b,ierr)
CALL HYPRE_StructVectorCreate(newcomm,grid,x,ierr)

! Indicate that the vector coefficients are ready to be set 

CALL HYPRE_StructVectorInitialize(b,ierr)
CALL HYPRE_StructVectorInitialize(x,ierr)

nvalues=mmx*mmy*mmz
ALLOCATE(values(nvalues)) 

! Set the values of b 

ict=1
DO k=1,mmz
  DO j=1,mmy
    DO i=1,mmx
      xloc=dx*DBLE(ilower(1)+i)
      yloc=dy*DBLE(ilower(2)+j)
      zloc=dz*DBLE(ilower(3)+k)
      CALL Source(values(ict),xloc,yloc,zloc,kx,ky,kz)
      ict=ict+1
    END DO
  END DO
END DO

CALL HYPRE_StructVectorSetBoxValues(b,ilower,iupper,values,ierr)

! Set x = 0

values=0.d0
CALL HYPRE_StructVectorSetBoxValues(x,ilower,iupper,values,ierr)

DEALLOCATE(values)

! Finalize the vector assembly

CALL HYPRE_StructVectorAssemble(b,ierr)
CALL HYPRE_StructVectorAssemble(x,ierr)

! Set up and use a solver 

! PCG parameters 

CALL HYPRE_StructPCGCreate(newcomm,solver,ierr)
CALL HYPRE_StructPCGSetMaxIter(solver,itmax,ierr)
CALL HYPRE_StructPCGSetTol(solver,ctol,ierr)
CALL HYPRE_StructPCGSetTwoNorm(solver,1,ierr)
CALL HYPRE_StructPCGSetRelChange(solver,0,ierr)
CALL HYPRE_StructPCGSetPrintLevel(solver,2,ierr)

! use symmetric SMG as preconditioner 

CALL HYPRE_StructSMGCreate(newcomm,precond,ierr)
CALL HYPRE_StructSMGSetMemoryUse(precond,0,ierr)
CALL HYPRE_StructSMGSetMaxIter(precond,1,ierr)
CALL HYPRE_StructSMGSetTol(precond,0.d0,ierr)
CALL HYPRE_StructSMGSetZeroGuess(precond,ierr)
CALL HYPRE_StructSMGSetNumPreRelax(precond,n_pre,ierr)
CALL HYPRE_StructSMGSetNumPostRelax(precond,n_post,ierr)
CALL HYPRE_StructSMGSetPrintLevel(precond,0,ierr)
CALL HYPRE_StructSMGSetLogging(precond,0,ierr)
CALL HYPRE_StructPCGSetPrecond(solver,solver_id,precond,ierr)

! PCG Setup

CALL HYPRE_StructPCGSetup(solver, A, b, x ,ierr)


! PCG Solve

CALL HYPRE_StructPCGSolve(solver,A,b,x,ierr)


! Get info and release memory

CALL HYPRE_StructPCGGetNumIterations(solver,num_iterations,ierr)
CALL HYPRE_StructPCGGetFinalRelative(solver,final_res_norm,ierr)
CALL HYPRE_StructPCGDestroy(solver,ierr)
CALL HYPRE_StructSMGDestroy(precond,ierr)

IF (myid == 0) THEN
  WRITE(19,*)"Iterations=",num_iterations
  WRITE(19,*)"Final Relative Residual Norm =",final_res_norm
END IF

IF (exsol) THEN
  nvalues = mmx*mmy*mmz
  ALLOCATE(values(nvalues))

! get the local solution

   CALL HYPRE_StructVectorGetBoxValues(x,ilower,iupper,values,ierr)
   errloc=0.d0
   sizeloc=0.d0
   ict=1
   DO k=1,mmz
     DO j=1,mmy
       DO i=1,mmx
         xloc=dx*DBLE(ilower(1)+i)
         yloc=dy*DBLE(ilower(2)+j)
         zloc=dz*DBLE(ilower(3)+k)
         CALL PoissonSol(uloc,xloc,yloc,zloc,kx,ky,kz)
         errloc=errloc+dx*dy*dz*(values(ict)-uloc)*(values(ict)-uloc)
         sizeloc=sizeloc+dx*dy*dz*uloc*uloc
         ict=ict+1
      END DO
    END DO
  END DO

  DEALLOCATE(values)
  CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  IF (myid==0) THEN
    WRITE(19,*)"Final relative error=",sqrt(err/size)
  END IF
END IF

IF (myid==0) THEN
  CLOSE(UNIT=19)
END IF

! Free memory

CALL HYPRE_StructGridDestroy(grid,ierr)
CALL HYPRE_StructStencilDestroy(stencil,ierr)
CALL HYPRE_StructMatrixDestroy(A,ierr)
CALL HYPRE_StructVectorDestroy(b,ierr)
CALL HYPRE_StructVectorDestroy(x,ierr)


CALL MPI_FINALIZE(ierr)

END PROGRAM Laplace_Hypre 

