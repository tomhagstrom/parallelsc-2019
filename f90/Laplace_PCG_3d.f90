PROGRAM Laplace_PCG_3d 

! Use second order differences and the preconditioned conjugate gradient method 
! to solve
!
! - div (sigma grad u) = f
!
! on the unit cube with Jacobi precnditioner

USE mpi
  
IMPLICIT NONE 

INTEGER, PARAMETER :: m=200  ! there will be mXm cells in the domain 
DOUBLE PRECISION :: ctol=1.d-6  ! solve until the residual norm < ctol - note that inner products will effectively 
                                ! scale by dx*dy 
DOUBLE PRECISION :: kx=2.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=4.d0  ! Wave number in y direction 
DOUBLE PRECISION :: kz=3.d0  ! Wave number in z direction 
LOGICAL :: exsol=.TRUE. ! We have an exact solution to check accuracy
LOGICAL :: precondition=.TRUE. ! If true precondition 
INTEGER :: itmax=10000 ! Quit if we need too many iterations 
DOUBLE PRECISION, DIMENSION(:,:,:), ALLOCATABLE :: u,w,p,r,q
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: psl,prl,psr,prr,psu,pru,psd,prd,psf,prf,psb,prb
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: x,y,z
DOUBLE PRECISION, DIMENSION(2) :: sigx,sigy,sigz  
DOUBLE PRECISION :: dx,dy,dz,err,size,errloc,sizeloc,ptw,ptwloc,qtr,qtrloc,qtrold,mu,nu,uloc  
DOUBLE PRECISION :: comptime,communtime,iotime,tc1,tc2,ti1,ti2,tt1,tt2 
INTEGER :: j,k,l,it,ierr,myid,numprocs,myup,mydown,myleft,myright,myfront,myback
INTEGER :: myxl,myxr,myyd,myyu,myzb,myzf,leftover,mmx,mmy,mmz 
INTEGER :: status_array(MPI_STATUS_SIZE,12),request(12) ! 12 different kinds of messages  
!
INTEGER, DIMENSION(3) :: dims,mycoords
LOGICAL, DIMENSION(3) :: isperiodic=(/ .FALSE. , .FALSE. , .FALSE. /) 
LOGICAL :: reorder=.TRUE.
INTEGER :: newcomm 

! We will call a subroutine to compute the forcing function 

INTERFACE 
  SUBROUTINE Source(floc,xloc,yloc,zloc,kx,ky,kz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz 
    DOUBLE PRECISION, INTENT(OUT) :: floc 
  END SUBROUTINE Source 
END INTERFACE 

! We will call another subroutine to evaluate the exact solution for testing
INTERFACE 
  SUBROUTINE PoissonSol(uloc,xloc,yloc,zloc,kx,ky,kz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,kx,ky,kz 
    DOUBLE PRECISION, INTENT(OUT) :: uloc 
  END SUBROUTINE PoissonSol 
END INTERFACE 

! And finally one for the diffusion coefficients
INTERFACE
  SUBROUTINE diffun(sigx,sigy,sigz,xloc,yloc,zloc,dx,dy,dz)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,zloc,dx,dy,dz
    DOUBLE PRECISION, DIMENSION(2), INTENT(OUT) :: sigx,sigy,sigz
  END SUBROUTINE diffun
END INTERFACE

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)

! Roughly we want to break the domain into squarish pieces - we use an MPI call to do it

CALL MPI_DIMS_CREATE(numprocs,3,dims,ierr)

CALL MPI_CART_CREATE(MPI_COMM_WORLD,3,dims,isperiodic,reorder,newcomm,ierr)

! Neighbors and coordinates

CALL MPI_COMM_RANK(newcomm,myid,ierr)
CALL MPI_CART_SHIFT(newcomm,0,1,myleft,myright,ierr)
CALL MPI_CART_SHIFT(newcomm,1,1,mydown,myup,ierr)
CALL MPI_CART_SHIFT(newcomm,2,1,myback,myfront,ierr)
CALL MPI_CART_COORDS(newcomm,myid,3,mycoords,ierr)

! Notice that the MPI_CART commands take care of not having a neighbor
! when we are at a boundary - but in the code I will be careful 

! Now we divvy out the data - as in shallow_water_mpi_Cart

leftover=MOD(m-1,dims(1)) 
mmx=(m-1)/dims(1)

IF (mmx < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(1) <= leftover) THEN
   myxl=(mmx+1)*mycoords(1)+1
ELSE
   myxl=(mmx+1)*leftover+mmx*(mycoords(1)-leftover)+1
END IF

IF (mycoords(1) < leftover) THEN
   myxr=myxl+mmx
ELSE
   myxr=myxl+mmx-1
END IF 

mmx=myxr-myxl+1 ! for later use

leftover=MOD(m-1,dims(2)) 
mmy=(m-1)/dims(2)

IF (mmy < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(2) <= leftover) THEN
   myyd=(mmy+1)*mycoords(2)+1
ELSE
   myyd=(mmy+1)*leftover+mmy*(mycoords(2)-leftover)+1
END IF

IF (mycoords(2) < leftover) THEN
   myyu=myyd+mmy
ELSE
   myyu=myyd+mmy-1
END IF 

mmy=myyu-myyd+1 ! for later use

leftover=MOD(m-1,dims(3)) 
mmz=(m-1)/dims(3)

IF (mmz < 3) THEN
  PRINT *,"Too many processors for the problem size"
  STOP
END IF

IF (mycoords(3) <= leftover) THEN
   myzb=(mmz+1)*mycoords(3)+1
ELSE
   myzb=(mmz+1)*leftover+mmz*(mycoords(3)-leftover)+1
END IF

IF (mycoords(3) < leftover) THEN
   myzf=myzb+mmz
ELSE
   myzf=myzb+mmz-1
END IF 

mmz=myzf-myzb+1 ! for later use

! Now we can allocate 

ALLOCATE(p(myxl-1:myxr+1,myyd-1:myyu+1,myzb-1:myzf+1),u(myxl:myxr,myyd:myyu,myzb:myzf), &
         w(myxl:myxr,myyd:myyu,myzb:myzf),r(myxl:myxr,myyd:myyu,myzb:myzf),q(myxl:myxr,myyd:myyu,myzb:myzf), &
         psl(mmy,mmz),prl(mmy,mmz),psr(mmy,mmz),prr(mmy,mmz),psu(mmx,mmz),pru(mmx,mmz), &
         psd(mmx,mmz),prd(mmx,mmz),psf(mmx,mmy),prf(mmx,mmy),psb(mmx,mmy),prb(mmx,mmy), &
         x(myxl:myxr),y(myyd:myyu),z(myzb:myzf)) 

! Notice I used the fact that we only need ghost values for p and we only need to exchange p 

! Set up the mesh

dx=1.d0/DBLE(m)
dy=dx
dz=dx

DO j=myxl,myxr
  x(j)=DBLE(j)*dx 
END DO

DO k=myyd,myyu
  y(k)=DBLE(k)*dy  
END DO

DO l=myzb,myzf
  z(l)=DBLE(l)*dz
END DO  

! Save basic input description - only use myid=0 for io 

IF (myid==0) THEN
  OPEN(UNIT=40,FILE="Laplace_PCG.case1",STATUS="UNKNOWN")
  WRITE(40,*)"m=",m
  IF (precondition) THEN
    WRITE(40,*)"Jacobi preconditioner"
  ELSE
    WRITE(40,*)"No preconditioning"
  END IF 
END IF 

! Initialize - the initial guess for u is zero so the initial residual is
! simply the source function 

DO l=myzb,myzf
  DO k=myyd,myyu
    DO j=myxl,myxr
      CALL Source(r(j,k,l),x(j),y(k),z(l),kx,ky,kz)
    END DO 
  END DO
END DO

p=0.d0
IF (precondition) THEN 
  DO l=myzb,myzf
    DO k=myyd,myyu
      DO j=myxl,myxr
        CALL diffun(sigx,sigy,sigz,x(j),y(k),z(l),dx,dy,dz) 
        q(j,k,l)=r(j,k,l)/((sigx(1)+sigx(2))/(dx*dx)+(sigy(1)+sigy(2))/(dy*dy)+(sigz(1)+sigz(2))/(dz*dz))
      END DO
    END DO
  END DO
ELSE
  q=r
END IF
p(myxl:myxr,myyd:myyu,myzb:myzf)=q(myxl:myxr,myyd:myyu,myzb:myzf)
u=0.d0 
w=0.d0 

! Now iterate - we start timing here

comptime=0.d0
iotime=0.d0
communtime=0.d0 

! I will use size to keep track of r^T r 

tc1=MPI_WTIME()

sizeloc=0.d0
DO l=myzb,myzf
  DO k=myyd,myyu
    DO j=myxl,myxr
      sizeloc=sizeloc+r(j,k,l)*r(j,k,l)
    END DO  
  END DO
END DO

IF (precondition) THEN 
  qtrloc=0.d0
  DO l=myzb,myzf
    DO k=myyd,myyu
      DO j=myxl,myxr
        qtrloc=qtrloc+q(j,k,l)*r(j,k,l)
      END DO  
    END DO
  END DO
END IF

tc2=MPI_WTIME()
comptime=comptime+tc2-tc1

tt1=MPI_WTIME()

CALL MPI_ALLREDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)

IF (precondition) THEN
  CALL MPI_ALLREDUCE(qtrloc,qtr,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)
ELSE
  qtr=size
END IF 

tt2=MPI_WTIME()
communtime=communtime+tt2-tt1

it=1

ti1=MPI_WTIME()

IF (myid==0) THEN
  WRITE (40,*)it,SQRT(size)
END IF 

ti2=MPI_WTIME() 
iotime=iotime+ti2-ti1

DO WHILE ((it <= itmax).AND.(SQRT(size) > ctol))

!
! Get data from my neighbors 
!
! I use nonblocking MPI calls - note that we make sure the tags match the message 
!

 tt1=MPI_WTIME()

 psu=p(myxl:myxr,myyu,myzb:myzf)
 psd=p(myxl:myxr,myyd,myzb:myzf)
 psr=p(myxr,myyd:myyu,myzb:myzf)
 psl=p(myxl,myyd:myyu,myzb:myzf)
 psf=p(myxl:myxr,myyd:myyu,myzf)
 psb=p(myxl:myxr,myyd:myyu,myzb)
 CALL MPI_IRECV(prd,mmx*mmz,MPI_DOUBLE_PRECISION,mydown,0,MPI_COMM_WORLD,request(1),ierr)
 CALL MPI_ISEND(psu,mmx*mmz,MPI_DOUBLE_PRECISION,myup,0,MPI_COMM_WORLD,request(2),ierr)
 CALL MPI_IRECV(pru,mmx*mmz,MPI_DOUBLE_PRECISION,myup,1,MPI_COMM_WORLD,request(3),ierr)
 CALL MPI_ISEND(psd,mmx*mmz,MPI_DOUBLE_PRECISION,mydown,1,MPI_COMM_WORLD,request(4),ierr)
 CALL MPI_IRECV(prl,mmy*mmz,MPI_DOUBLE_PRECISION,myleft,2,MPI_COMM_WORLD,request(5),ierr)
 CALL MPI_ISEND(psr,mmy*mmz,MPI_DOUBLE_PRECISION,myright,2,MPI_COMM_WORLD,request(6),ierr)
 CALL MPI_IRECV(prr,mmy*mmz,MPI_DOUBLE_PRECISION,myright,3,MPI_COMM_WORLD,request(7),ierr)
 CALL MPI_ISEND(psl,mmy*mmz,MPI_DOUBLE_PRECISION,myleft,3,MPI_COMM_WORLD,request(8),ierr)
 CALL MPI_IRECV(prb,mmx*mmy,MPI_DOUBLE_PRECISION,myback,4,MPI_COMM_WORLD,request(9),ierr)
 CALL MPI_ISEND(psf,mmx*mmy,MPI_DOUBLE_PRECISION,myfront,4,MPI_COMM_WORLD,request(10),ierr)
 CALL MPI_IRECV(prf,mmx*mmy,MPI_DOUBLE_PRECISION,myfront,5,MPI_COMM_WORLD,request(11),ierr)
 CALL MPI_ISEND(psb,mmx*mmy,MPI_DOUBLE_PRECISION,myback,5,MPI_COMM_WORLD,request(12),ierr)
!
! Now we wait
!
 CALL MPI_WAITALL(12,request,status_array,ierr)
!
 IF (mycoords(1) < (dims(1)-1)) THEN
   p(myxr+1,myyd:myyu,myzb:myzf)=prr
 ELSE
   p(myxr+1,myyd:myyu,myzb:myzf)=0.d0
 END IF 
 IF (mycoords(1) > 0) THEN
   p(myxl-1,myyd:myyu,myzb:myzf)=prl
 ELSE
   p(myxl-1,myyd:myyu,myzb:myzf)=0.d0 
 END IF 
 IF (mycoords(2) < (dims(2)-1))  THEN
   p(myxl:myxr,myyu+1,myzb:myzf)=pru
 ELSE
   p(myxl:myxr,myyu+1,myzb:myzf)=0.d0
 END IF
 IF (mycoords(2) > 0) THEN 
   p(myxl:myxr,myyd-1,myzb:myzf)=prd
 ELSE
   p(myxl:myxr,myyd-1,myzb:myzf)=0.d0
 END IF 
 IF (mycoords(3) < (dims(3)-1))  THEN
   p(myxl:myxr,myyd:myyu,myzf+1)=prf
 ELSE
   p(myxl:myxr,myyd:myyu,myzf+1)=0.d0 
 END IF
 IF (mycoords(3) > 0) THEN 
   p(myxl:myxr,myyd:myyu,myzb-1)=prb
 ELSE
   p(myxl:myxr,myyd:myyu,myzb-1)=0.d0 
 END IF 
!
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1
!
 tc1=MPI_WTIME()
 
 w=0.d0
 DO l=myzb,myzf
   DO k=myyd,myyu
     DO j=myxl,myxr
       CALL diffun(sigx,sigy,sigz,x(j),y(k),z(l),dx,dy,dz)
       w(j,k,l)=-(sigx(2)*(p(j+1,k,l)-p(j,k,l))-sigx(1)*(p(j,k,l)-p(j-1,k,l)))/(dx*dx) &
                -(sigy(2)*(p(j,k+1,l)-p(j,k,l))-sigy(1)*(p(j,k,l)-p(j,k-1,l)))/(dy*dy) &
                -(sigz(2)*(p(j,k,l+1)-p(j,k,l))-sigz(1)*(p(j,k,l)-p(j,k,l-1)))/(dz*dz) 
     END DO 
   END DO
 END DO

 ptwloc=0.d0
 DO l=myzb,myzf
   DO k=myyd,myyu
     DO j=myxl,myxr
       ptwloc=ptwloc+p(j,k,l)*w(j,k,l)
     END DO 
   END DO
 END DO

 tc2=MPI_WTIME()
 comptime=comptime+tc2-tc1
 
 tt1=MPI_WTIME()

 CALL MPI_ALLREDUCE(ptwloc,ptw,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)
  
 tt2=MPI_WTIME()
 communtime=communtime+tt2-tt1

 tc1=MPI_WTIME()

 nu=qtr/ptw

 DO l=myzb,myzf
   DO k=myyd,myyu
     DO j=myxl,myxr
       u(j,k,l)=u(j,k,l)+nu*p(j,k,l)
     END DO
   END DO
 END DO

 DO l=myzb,myzf
   DO k=myyd,myyu
     DO j=myxl,myxr
       r(j,k,l)=r(j,k,l)-nu*w(j,k,l)
     END DO 
   END DO
 END DO

  IF (precondition) THEN 
    DO l=myzb,myzf
      DO k=myyd,myyu
        DO j=myxl,myxr
          CALL diffun(sigx,sigy,sigz,x(j),y(k),z(l),dx,dy,dz) 
          q(j,k,l)=r(j,k,l)/((sigx(1)+sigx(2))/(dx*dx)+(sigy(1)+sigy(2))/(dy*dy)+(sigz(1)+sigz(2))/(dz*dz))
        END DO
      END DO
    END DO
  ELSE
    q=r
  END IF

  qtrold=qtr

  sizeloc=0.d0
  DO l=myzb,myzf
    DO k=myyd,myyu
      DO j=myxl,myxr
        sizeloc=sizeloc+r(j,k,l)*r(j,k,l)
      END DO  
    END DO
  END DO

  IF (precondition) THEN 
    qtrloc=0.d0
    DO l=myzb,myzf
      DO k=myyd,myyu
        DO j=myxl,myxr
          qtrloc=qtrloc+q(j,k,l)*r(j,k,l)
        END DO  
      END DO
    END DO
  END IF

  tc2=MPI_WTIME()
  comptime=comptime+tc2-tc1

  tt1=MPI_WTIME()

  CALL MPI_ALLREDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)

  IF (precondition) THEN
    CALL MPI_ALLREDUCE(qtrloc,qtr,1,MPI_DOUBLE_PRECISION,MPI_SUM,newcomm,ierr)
  ELSE
    qtr=size
  END IF 

  tt2=MPI_WTIME()
  communtime=communtime+tt2-tt1

  tc1=MPI_WTIME()

  mu=qtr/qtrold

 DO l=myzb,myzf
   DO k=myyd,myyu
     DO j=myxl,myxr
       p(j,k,l)=q(j,k,l)+mu*p(j,k,l)
     END DO
   END DO
 END DO

 it=it+1

 ti1=MPI_WTIME()

 IF (myid==0) THEN
   WRITE (40,*)it,SQRT(size)
 END IF 

 ti2=MPI_WTIME() 
 iotime=iotime+ti2-ti1

END DO

ti1=MPI_WTIME()

IF (exsol) THEN
  errloc=0.d0
  sizeloc=0.d0
  DO l=myzb,myzf
    DO k=myyd,myyu
      DO j=myxl,myxr
        CALL PoissonSol(uloc,x(j),y(k),z(l),kx,ky,kz)
        errloc=errloc+dx*dy*dz*(u(j,k,l)-uloc)**2
        sizeloc=sizeloc+dx*dy*dz*uloc**2
      END DO
    END DO
  END DO
  CALL MPI_REDUCE(errloc,err,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  IF (myid==0) THEN
    WRITE(40,*)"Final error:",SQRT(err/size)
  END IF
ELSE 
  sizeloc=0.d0 
  DO l=myzb,myzf
    DO k=myyd,myyu
      DO j=myxl,myxr
        sizeloc=sizeloc+dx*dy*dz*u(j,k,l)**2
      END DO
    END DO
  END DO
  CALL MPI_REDUCE(sizeloc,size,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,newcomm,ierr)
  IF (myid==0) THEN
    WRITE(40,*)"Solution size:",SQRT(size)
  END IF
END IF

ti2=MPI_WTIME()
iotime=iotime+ti2-ti1
   
IF (myid==0) THEN
  CLOSE(UNIT=29) 
END IF

! Done 

IF (myid==0) THEN
  PRINT *,"Computation time=",comptime
  PRINT *,"IO time =",iotime
  PRINT *,"Communication time=",communtime
END IF 

CALL MPI_FINALIZE(ierr)

END PROGRAM Laplace_PCG_3d   
