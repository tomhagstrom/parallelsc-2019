PROGRAM gravity

! Solve for the motion of N bodies using Newton's laws

USE omp_lib
USE mpi 

IMPLICIT NONE

INTEGER, PARAMETER :: N=10000 ! number of bodies
DOUBLE PRECISION :: ttot=10.d0
INTEGER :: nsteps=10000
DOUBLE PRECISION, DIMENSION(N) :: x,y,z,m
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: myx,myy,myz,mym,Vx,Vy,Vz,Fx,Fy,Fz
INTEGER, DIMENSION(:), ALLOCATABLE :: disps,counts
DOUBLE PRECISION :: dt
REAL comp_time,start_time,finish_time
INTEGER :: j,it,myN,myid,numprocs,leftover,ierr 

dt=ttot/DBLE(nsteps)

! Set up the communicator

CALL MPI_INIT(ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,numprocs,ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD,myid,ierr)

! Break up the problem

leftover=MOD(N,numprocs)
myN=N/numprocs
IF (myid < leftover) THEN
   myN=myN+1
END IF

! Allocate arrays

ALLOCATE(myx(myN),myy(myN),myz(myN),mym(myN),Vx(myN),Vy(myN),Vz(myN),Fx(myN),Fy(myN),Fz(myN))
ALLOCATE(counts(numprocs),disps(numprocs))

! Find out how many particles everyone has and create disps array
! We send and receive one integer to and from everyone

CALL MPI_ALLGATHER(myN,1,MPI_INTEGER,counts,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

disps(1)=0
DO j=2,numprocs
  disps(j)=counts(j-1)+disps(j-1)
END DO    

CALL Initialize

! Send the masses to everyone

CALL MPI_ALLGATHERV(mym,myN,MPI_DOUBLE_PRECISION,m,counts,disps, &
                   MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ierr)

IF (myid==0) THEN
  CALL cpu_time(start_time)
END IF  

DO it=1,nsteps
  ! First the velocity step - first we must send positions all around
  CALL MPI_ALLGATHERV(myx,myN,MPI_DOUBLE_PRECISION,x,counts,disps, &
                     MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ierr)
  CALL MPI_ALLGATHERV(myy,myN,MPI_DOUBLE_PRECISION,y,counts,disps, &
                     MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ierr)
  CALL MPI_ALLGATHERV(myz,myN,MPI_DOUBLE_PRECISION,z,counts,disps, &
                     MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ierr)
   
  CALL Force
  !$OMP PARALLEL DO SHARED(Vx,Vy,Vz,Fx,Fy,Fz)
  DO j=1,myN
    Vx(j)=Vx(j)+dt*Fx(j)
    Vy(j)=Vy(j)+dt*Fy(j)
    Vz(j)=Vz(j)+dt*Fz(j)
  END DO
  !$OMP END PARALLEL DO
  ! Now the position step
  !$OMP PARALLEL DO SHARED(x,y,z,Vx,Vy,Vz)
  DO j=1,myN
    myx(j)=myx(j)+dt*Vx(j)
    myy(j)=myy(j)+dt*Vy(j)
    myz(j)=myz(j)+dt*Vz(j)
  END DO
  !$OMP END PARALLEL DO
END DO

IF (myid==0) THEN
  CALL cpu_time(finish_time)
  comp_time=finish_time-start_time
  PRINT *,comp_time
END IF 

! Gather positions on myid=0 for output

CALL MPI_GATHERV(myx,myN,MPI_DOUBLE_PRECISION,x,counts,disps, &
                 MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
CALL MPI_GATHERV(myy,myN,MPI_DOUBLE_PRECISION,y,counts,disps, &
                 MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
CALL MPI_GATHERV(myz,myN,MPI_DOUBLE_PRECISION,z,counts,disps, &
                 MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

IF (myid==0) THEN
  OPEN(UNIT=29,FILE="Particle_Positions_10k",STATUS="UNKNOWN")
  DO j=1,N
    WRITE(29,*)x(j),y(j),z(j)
  END DO
  CLOSE(UNIT=29)
END IF 

CONTAINS

SUBROUTINE Force
!
DOUBLE PRECISION :: dx,dy,dz,rad,ris
DOUBLE PRECISION :: small=1.d-20 ! Avoid ifs in loops
INTEGER :: j,k

!$OMP PARALLEL DO PRIVATE(dx,dy,dz,rad,ris,k) SHARED(myx,myy,myz,x,y,z,m,Fx,Fy,Fz)
DO j=1,myN
  Fx(j)=0.d0
  Fy(j)=0.d0
  Fz(j)=0.d0
  DO k=1,N
    dx=x(k)-myx(j)
    dy=y(k)-myy(j)
    dz=z(k)-myz(j)
    rad=SQRT(dx*dx+dy*dy+dz*dz+small)
    ris=m(k)/(rad*rad*rad)
    Fx(j)=Fx(j)+dx*ris
    Fy(j)=Fy(j)+dy*ris
    Fz(j)=Fz(j)+dz*ris
  END DO
END DO
!$OMP END PARALLEL DO
!
END SUBROUTINE Force

SUBROUTINE Initialize
!
DOUBLE PRECISION :: pi=3.1415926535897932385d0
DOUBLE PRECISION :: mbar=5.d0
DOUBLE PRECISION :: Vbar=3.d0
DOUBLE PRECISION :: R=5.d0
!
! Choose random positions in the sphere of radius R
! with random masses between 1 and 9 and random velocities
! biased by Vbar in the radial direction
!
DOUBLE PRECISION :: rad,theta,phi,rnum
INTEGER :: j,nrand
INTEGER, DIMENSION(:), ALLOCATABLE :: seed
!
CALL random_seed(size=nrand)
ALLOCATE(seed(nrand))
DO j=1,nrand
  seed(j)=j+myid*223
END DO
CALL random_seed(put=seed) 
!
!$OMP PARALLEL DO PRIVATE(rad,theta,phi,rnum) SHARED(myx,myy,myz,Vx,Vy,Vz,mym)

DO j=1,N
  CALL random_number(rnum)
  rad=R*rnum
  CALL random_number(rnum)
  theta=pi*rnum
  CALL random_number(rnum)
  phi=2.d0*pi*rnum
  x(j)=rad*SIN(theta)*COS(phi)
  y(j)=rad*SIN(theta)*SIN(phi)
  z(j)=rad*COS(theta)
  CALL random_number(rnum)
  Vx(j)=Vbar*x(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  Vy(j)=Vbar*y(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  Vz(j)=Vbar*z(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  m(j)=mbar+8.d0*rnum-4.d0
END DO
!$OMP END PARALLEL DO

END SUBROUTINE Initialize

END PROGRAM gravity 
