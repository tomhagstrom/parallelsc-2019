PROGRAM gravity

! Solve for the motion of N bodies using Newton's laws

USE omp_lib

IMPLICIT NONE

INTEGER, PARAMETER :: N=1000 ! number of bodies
DOUBLE PRECISION :: ttot=10.d0
INTEGER :: nsteps=10000
DOUBLE PRECISION, DIMENSION(N) :: x,y,z,Vx,Vy,Vz,m,Fx,Fy,Fz
DOUBLE PRECISION :: dt
REAL comp_time,start_time,finish_time
INTEGER :: j,it

dt=ttot/DBLE(nsteps)

CALL Initialize

CALL cpu_time(start_time)

DO it=1,nsteps
  ! First the velocity step
  CALL Force
  !$OMP PARALLEL DO SHARED(Vx,Vy,Vz,Fx,Fy,Fz)
  DO j=1,N
    Vx(j)=Vx(j)+dt*Fx(j)
    Vy(j)=Vy(j)+dt*Fy(j)
    Vz(j)=Vz(j)+dt*Fz(j)
  END DO
  !$OMP END PARALLEL DO
  ! Now the position step
  !$OMP PARALLEL DO SHARED(x,y,z,Vx,Vy,Vz)
  DO j=1,N
    x(j)=x(j)+dt*Vx(j)
    y(j)=y(j)+dt*Vy(j)
    z(j)=z(j)+dt*Vz(j)
  END DO
  !$OMP END PARALLEL DO
END DO

CALL cpu_time(finish_time)
comp_time=finish_time-start_time
PRINT *,comp_time

OPEN(UNIT=29,FILE="Particle_Positions",STATUS="UNKNOWN")
DO j=1,N
  WRITE(29,*)x(j),y(j),z(j)
END DO

CLOSE(UNIT=29)

CONTAINS

SUBROUTINE Force
!
DOUBLE PRECISION :: dx,dy,dz,rad,ris
DOUBLE PRECISION :: small=1.d-20 ! Avoid ifs in loops
INTEGER :: j,k

!$OMP PARALLEL DO PRIVATE(dx,dy,dz,rad,ris,k) SHARED(x,y,z,m,Fx,Fy,Fz)
DO j=1,N
  Fx(j)=0.d0
  Fy(j)=0.d0
  Fz(j)=0.d0
  DO k=1,N
    dx=x(k)-x(j)
    dy=y(k)-y(j)
    dz=z(k)-z(j)
    rad=SQRT(dx*dx+dy*dy+dz*dz+small)
    ris=m(k)/(rad*rad*rad)
    Fx(j)=Fx(j)+dx*ris
    Fy(j)=Fy(j)+dy*ris
    Fz(j)=Fz(j)+dz*ris
  END DO
END DO
!$OMP END PARALLEL DO
!
END SUBROUTINE Force

SUBROUTINE Initialize
!
DOUBLE PRECISION :: pi=3.1415926535897932385d0
DOUBLE PRECISION :: mbar=5.d0
DOUBLE PRECISION :: Vbar=3.d0
DOUBLE PRECISION :: R=5.d0
!
! Choose random positions in the sphere of radius R
! with random masses between 1 and 9 and random velocities
! biased by Vbar in the radial direction
!
DOUBLE PRECISION :: rad,theta,phi,rnum
INTEGER :: j
!
!$OMP PARALLEL DO PRIVATE(rad,theta,phi,rnum) SHARED(x,y,z,Vx,Vy,Vz,m)
DO j=1,N
  CALL random_number(rnum)
  rad=R*rnum
  CALL random_number(rnum)
  theta=pi*rnum
  CALL random_number(rnum)
  phi=2.d0*pi*rnum
  x(j)=rad*SIN(theta)*COS(phi)
  y(j)=rad*SIN(theta)*SIN(phi)
  z(j)=rad*COS(theta)
  CALL random_number(rnum)
  Vx(j)=Vbar*x(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  Vy(j)=Vbar*y(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  Vz(j)=Vbar*z(j)+6.d0*rnum-3.d0 
  CALL random_number(rnum)
  m(j)=mbar+8.d0*rnum-4.d0
END DO
!$OMP END PARALLEL DO

END SUBROUTINE Initialize

END PROGRAM gravity 