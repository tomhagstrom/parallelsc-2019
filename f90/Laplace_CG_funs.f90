SUBROUTINE Source(floc,xloc,yloc,kx,ky)
  DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky
  DOUBLE PRECISION, INTENT(OUT) :: floc 
!
  DOUBLE PRECISION :: pi=3.1415926535897932385d0
!
  floc=(SIN(kx*pi*xloc)*SIN(ky*pi*yloc)*(pi*pi*(kx*kx+ky*ky)-(xloc*xloc+yloc*yloc)) &
       -2.d0*pi*(kx*yloc*COS(kx*pi*xloc)*SIN(ky*pi*yloc)+ky*xloc*SIN(kx*pi*xloc)*COS(ky*pi*yloc)))*EXP(xloc*yloc)

END SUBROUTINE Source 

SUBROUTINE PoissonSol(uloc,xloc,yloc,kx,ky)
  DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky
  DOUBLE PRECISION, INTENT(OUT) :: uloc 
!
  DOUBLE PRECISION :: pi=3.1415926535897932385d0
!
  uloc=SIN(kx*pi*xloc)*SIN(ky*pi*yloc)*EXP(xloc*yloc)
!
END SUBROUTINE PoissonSol 



